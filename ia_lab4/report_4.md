---
title : "Report Lab 4 - Planning"
author : Loïs Bilat, Philippe Hérail
date : \today
...

# Task 1
We implemented two of the proposed alternative for this task. We did Alternative 3, the imporvement of logistics++ and alternative 4 Emergency Systems Logistics.

## Logistics ++

The first step was to implement the trains. To do that, we added in the domain file the following predicates.
```lisp
(train ?t) (station ?s)
```
We also added a new action named **ride** allowing trains to go from a station to another one.

```lisp
(:action ride
    :parameters (?t ?s1 ?s2)
    :precondition (and (train ?t) (station ?s1) (station ?s2)
               (at ?t ?s1))
    :effect (and (at ?t ?s2) (not (at ?t ?s1))))
)
```

This is basically similar to the action fly, we just changed the three preconditions `(airplane ?p) (airport ?a1) (airport ?a2)` by `(train ?t) (station ?s1) (station ?s2)`.

In the problem file, we simply initialized all trains and stations correctly, the same way the airplanes and airports were initialized.

To handle different sizes of packets, we added the following predicates to the domain file 

```lisp
(format ?s)
(size ?p ?s)
(space ?v ?s)
```

`format` will designate a size (small, medium or big). `(size ?p ?s)` represent the size of a packet. `(space ?v ?s)` tells if a vehicle ``?v`` can transport a packet of size ``?s``.

After that, we had to modify the *load* action to only be able to pick packets of acceptable sizes. We simply added `(size ?o ?s) (space ?v ?s)` as preconditions. That way, we can only pick a packet of size `?s` if the vehicle accepts packets of size `?s`.

In the problem file, we defined the objects `small medium big` as instances of size using ` (format small) (format medium) (format big)`. We then gave each packet a specific size, and each vehicle a list of acceptable sizes.

````lisp
(size packet1 small)
(size packet2 big)
(size packet3 medium)
(size packet4 medium)

(space truck1 big) (space truck1 medium) (space truck1 small)
(space truck2 big) (space truck2 medium) (space truck2 small)
(space truck3 big) (space truck3 medium) (space truck3 small)
(space truck4 big) (space truck4 medium) (space truck4 small)

(space airplane1 big) (space airplane1 medium) (space airplane1 small)
(space airplane2 small)

etc...
````

Each vehicle that can accept big packets can also accept small and medium packets. We need to specifically write that.

## Emergency Delivery system

To create the domain, we defined 5 types of objects. *crate, helicopter, location, depot, person*, each depot and person being a location. We then added 4 predicates.

```lisp
(empty ?e)     ;; ?e (helicopter) is empty
(at ?h ?l)     ;; ?h (helicopter) is at location ?l
(in ?c ?h)     ;; ?c (crate) is in helicopter ?h
(at_loc ?c ?l) ;; c? (crate) is at location ?l
```

We needed 4 actions: *load_depot, load, unload, fly*. We distinguished two different loads, one for when we load from a depot, that will allow us to represent the fact that we have "unlimited" crates at the depot, and one that loads from a person, in which case the crate will disappear from the location.

```lisp
(:action load_depot
    :parameters (?c ?h ?l)
    :precondition (and (crate ?c) (helicopter ?h) (location ?l) 
                     (depot ?l) (at ?h ?l) (empty ?h) (at_loc ?c ?l))
    :effect (and (not (empty ?h)) (in ?c ?h))
)
```

```lisp
(:action load
    :parameters (?c ?h ?l)
    :precondition (and (crate ?c) (helicopter ?h) (location ?l) 
                     (person ?l) (at ?h ?l) (empty ?h) (at_loc ?c ?l))
    :effect (and (not (empty ?h)) (not (at_loc?c ?l)) (in ?c ?h))
)
```

We see that the only differences is `(depot ?d) versus (person ?p)` and the effect `(not (at_loc?c ?l))` that is added when we pick a crate from a person. In this simple representation, there is no reason to pick a crate from a person, but we might imagine another more advanced version where distances are represented and one person might be used as an "alternative depot" to optimize the travel time of the helicopters.

The implementation of *unload* and *fly* are straightforward. Unload checks that the helicopter indeed has a crate inside, and unload it. Flying goes from a location to another one, without worrying about the content of the helicopter.

```lisp
(:action unload
    :parameters (?c ?h ?l)
    :precondition (and (crate ?c) (helicopter ?h) (location ?l) 
                            (person ?l) (at ?h ?l) (not(empty ?h)) (in ?c ?h))
    :effect (and (empty ?h) (at_loc?c ?l) (not (in ?c ?h)))
)
```

```lisp
(:action fly
    :parameters (?h ?l1 ?l2)
    :precondition (and (helicopter ?h) (location ?l1) (location ?l2)
                             (at ?h ?l1) )
    :effect (and (not (at ?h ?l1)) (at ?h ?l2))
)
```

In the problem definition file, we did the following. We created some types of crate (*water, food, medicine, clothes*), a depot, 2 helicopters and 4 people. Each *depot* and *person* was also declared as a location. Every helicopter is initially empty, and at the depot. Each create is also at the depot. As a goal, we represented the goal of people using the *at_loc*  predicate. For example, if person2 need water and medicine, we represent it as `(and (at_loc water person2) (at_loc medicine person2))`.

# Task 2

## Analysis of different configurations

We ran problem 02 and problem 03 on each of the 3 configurations, and we obtained the following results.

### Problem 02

#### FF

The results are the following.

```
Plan length: 22 step(s).
Plan cost: 22
Expanded 27 state(s).
Reopened 0 state(s).
Evaluated 151 state(s).
Evaluations: 151
Generated 171 state(s).
Dead ends: 13 state(s).
Number of registered states: 151
Search time: 0.0319655s
Total time: 0.0724665s
Solution found.
Peak memory: 4944 KB
```

#### GC

The results are the following.

```
Plan length: 25 step(s).
Plan cost: 25
Expanded 12222 state(s).
Reopened 0 state(s).
Evaluated 19627 state(s).
Evaluations: 19627
Generated 38284 state(s).
Dead ends: 0 state(s).
Number of registered states: 19627
Search time: 5.57758s
Total time: 5.60888s
Solution found.
Peak memory: 35620 KB
```

#### GC with FF actions

The results are the following.

```
Plan length: 25 step(s).
Plan cost: 25
Expanded 1575 state(s).
Reopened 0 state(s).
Evaluated 4620 state(s).
Evaluations: 6194
Generated 6197 state(s).
Dead ends: 0 state(s).
Number of registered states: 4620
Search time: 0.701271s
Total time: 0.737252s
Solution found.
Peak memory: 12016 KB
```

We see that on this problem, the fastest configuration is the Fast-Forward one. Not only it found a better solution, but it also found it much faster and using less memory. It only expanded 5 useless states, whereas the other ones expanded thousands of useless states.

### Problem 03

#### FF

The results are the following.

```
Plan length: 31 step(s).
Plan cost: 31
Expanded 1297 state(s).
Reopened 0 state(s).
Evaluated 5248 state(s).
Evaluations: 5248
Generated 7347 state(s).
Dead ends: 2284 state(s).
Number of registered states: 5248
Search time: 1.33935s
Total time: 1.40111s
Solution found.
Peak memory: 13788 KB
```

#### GC

The results are the following.

```
Plan length: 30 step(s).
Plan cost: 30
Expanded 320 state(s).
Reopened 0 state(s).
Evaluated 1154 state(s).
Evaluations: 1154
Generated 1357 state(s).
Dead ends: 0 state(s).
Number of registered states: 1154
Search time: 0.126699s
Total time: 0.190136s
Solution found.
Peak memory: 6812 KB
```

#### GC with FF actions

The results are the following.
```
Plan length: 30 step(s).
Plan cost: 30
Expanded 249 state(s).
Reopened 0 state(s).
Evaluated 953 state(s).
Evaluations: 1201
Generated 1115 state(s).
Dead ends: 0 state(s).
Number of registered states: 953
Search time: 0.109023s
Total time: 0.179453s
Solution found.
Peak memory: 6924 KB
```

On this problem, the result is the opposite of the problem 02. FF is the slowest and most memory consuming configuration, and it found a worse solution than the other two. The GC configuration with FF functions is the best, but not by much compared to the GC configuration.

## Comparing heuristics, problem 03


When running the Fast downward visualizer twice using the generated log files, we obtains the following graphs.

For a), the FF-heuristic

![Graph](p03_v1/graph.png)

For b), the goal count heuristic

![Graph](p03_v2/graph.png)

We can see that the goal count heuristic is clearly the most efficient, it explores way less states. During the expansion, we can see that the expanded nodes are "in the correct direction". The FF-heuristic takes much more time, and explores a lot of states that aren't close at all from the goal node.

Both configurations use the same king of actions, which is normal since the problem is the same. They obviously don't use the same in the same order, simply because they both find another solution.

On the first path generated, we discover a path approximately 50 nodes long before exploring other pars of the graph. In the final solution, only the 12 first nodes of the original long path are being used.

## Comparing heuristics, problem 02
 
For the planner using FF, the first node with a smaller heuristic value appears at time step 1. For the planner using GC with preferred operators from FF, the first node with a smaller value appears at time step 19.

At time step 27, there are still 6 goals left to solve according to the goal count heuristic.

When we follow the path found as a solution, the value of the goal count heuristic never increase from a node to the next one.

## Graph analysis on our own problem

We ran the graph application on both the FF and GC configurations of the emergency delivery problem. The resulting graphs are very similar. The only major difference is the number of successors for a node, bigger in our case than in problem 02 an 03. They are similar because the nature of the problem is similar. In both problems we need to deliver packets somewhere, each time with limitations on the capacity and on different needs for each destination.
