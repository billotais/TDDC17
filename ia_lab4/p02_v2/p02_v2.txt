INFO     Running translator.
INFO     translator input: ['domain.pddl', 'p02.pddl']
INFO     translator arguments: []
INFO     translator time limit: None
INFO     translator memory limit: None
INFO     callstring: /usr/bin/python /home/TDDC17/sw/fdlog/builds/release32/bin/translate/translate.py domain.pddl p02.pddl
Parsing...
Parsing: [0.110s CPU, 0.106s wall-clock]
Normalizing task... [0.000s CPU, 0.001s wall-clock]
Instantiating...
Generating Datalog program... [0.020s CPU, 0.015s wall-clock]
Normalizing Datalog program...
Normalizing Datalog program: [0.000s CPU, 0.003s wall-clock]
Preparing model... [0.070s CPU, 0.074s wall-clock]
Generated 23 rules.
Computing model... [0.220s CPU, 0.220s wall-clock]
7333 relevant atoms
12880 auxiliary atoms
20213 final queue length
23431 total queue pushes
Completing instantiation... [0.110s CPU, 0.107s wall-clock]
Instantiating: [0.430s CPU, 0.427s wall-clock]
Computing fact groups...
Finding invariants...
9 initial candidates
Finding invariants: [0.000s CPU, 0.004s wall-clock]
Checking invariant weight... [0.010s CPU, 0.003s wall-clock]
Instantiating groups... [0.000s CPU, 0.003s wall-clock]
Collecting mutex groups... [0.000s CPU, 0.000s wall-clock]
Choosing groups...
0 uncovered facts
Choosing groups: [0.000s CPU, 0.000s wall-clock]
Building translation key... [0.000s CPU, 0.000s wall-clock]
Computing fact groups: [0.010s CPU, 0.013s wall-clock]
Building STRIPS to SAS dictionary... [0.000s CPU, 0.000s wall-clock]
Building dictionary for full mutex groups... [0.000s CPU, 0.000s wall-clock]
Building mutex information...
Building mutex information: [0.000s CPU, 0.000s wall-clock]
Translating task...
Processing axioms...
Simplifying axioms... [0.000s CPU, 0.000s wall-clock]
Processing axioms: [0.010s CPU, 0.004s wall-clock]
Translating task: [0.090s CPU, 0.086s wall-clock]
0 effect conditions simplified
0 implied preconditions added
Detecting unreachable propositions...
0 operators removed
0 axioms removed
9 propositions removed
Detecting unreachable propositions: [0.050s CPU, 0.052s wall-clock]
Reordering and filtering variables...
9 of 9 variables necessary.
0 of 9 mutex groups necessary.
1732 of 1732 operators necessary.
0 of 0 axiom rules necessary.
Reordering and filtering variables: [0.010s CPU, 0.008s wall-clock]
Translator variables: 9
Translator derived variables: 0
Translator facts: 157
Translator goal facts: 7
Translator mutex groups: 0
Translator total mutex groups size: 0
Translator operators: 1732
Translator axioms: 0
Translator task size: 8735
Translator peak memory: 62056 KB
Writing output... [0.030s CPU, 0.039s wall-clock]
Done! [0.730s CPU, 0.738s wall-clock]
INFO     Running search (release32).
INFO     search input: output.sas
INFO     search arguments: ['--heuristic', 'gc=goalcount()', '--search', 'eager_greedy([gc])']
INFO     search time limit: None
INFO     search memory limit: None
INFO     search executable: /home/TDDC17/sw/fdlog/builds/release32/bin/downward
INFO     callstring: /home/TDDC17/sw/fdlog/builds/release32/bin/downward --heuristic 'gc=goalcount()' --search 'eager_greedy([gc])' --internal-plan-file sas_plan < output.sas
reading input... [t=9.0858e-05s]
done reading input! [t=0.0293124s]
packing state variables...done! [t=0.0293424s]
Variables: 9
FactPairs: 157
Bytes per state: 4
Building successor generator...done! [t=0.0307419s]
done initializing global data [t=0.0307497s]
Initializing goal count heuristic...
Conducting best first search without reopening closed nodes, (real) bound = 2147483647
New best heuristic value for goalcount: 7
[g=0, 1 evaluated, 0 expanded, t=0.0314648s, 4456 KB]
Initial heuristic value for goalcount: 7
pruning method: none
New best heuristic value for goalcount: 6
[g=3, 65 evaluated, 19 expanded, t=0.0402422s, 4588 KB]
New best heuristic value for goalcount: 5
[g=6, 127 evaluated, 35 expanded, t=0.0481053s, 4716 KB]
New best heuristic value for goalcount: 4
[g=10, 366 evaluated, 101 expanded, t=0.0773451s, 4972 KB]
New best heuristic value for goalcount: 3
[g=14, 504 evaluated, 146 expanded, t=0.0931409s, 5228 KB]
New best heuristic value for goalcount: 2
[g=19, 604 evaluated, 189 expanded, t=0.104122s, 5356 KB]
New best heuristic value for goalcount: 1
[g=22, 822 evaluated, 322 expanded, t=0.134906s, 5740 KB]
New best heuristic value for goalcount: 0
[g=25, 19627 evaluated, 12221 expanded, t=5.59624s, 35620 KB]
Solution found!
Actual search time: 5.56476s [t=5.59634s]
load p5 t0 l5 (1)
drive t0 l5 l0 level93 level6 level99 (1)
unload p5 t0 l0 (1)
load p4 t0 l0 (1)
drive t0 l0 l3 level75 level18 level93 (1)
unload p4 t0 l3 (1)
load p1 t0 l3 (1)
drive t0 l3 l1 level63 level12 level75 (1)
drive t0 l1 l2 level60 level3 level63 (1)
unload p1 t0 l2 (1)
load p3 t0 l2 (1)
drive t0 l2 l1 level57 level3 level60 (1)
drive t0 l1 l3 level45 level12 level57 (1)
load p0 t0 l3 (1)
drive t0 l3 l4 level37 level8 level45 (1)
load p6 t0 l4 (1)
load p2 t0 l4 (1)
drive t0 l4 l5 level25 level12 level37 (1)
drive t0 l5 l0 level19 level6 level25 (1)
unload p6 t0 l0 (1)
unload p3 t0 l0 (1)
unload p2 t0 l0 (1)
drive t0 l0 l5 level13 level6 level19 (1)
drive t0 l5 l2 level0 level13 level13 (1)
unload p0 t0 l2 (1)
Plan length: 25 step(s).
Plan cost: 25
Expanded 12222 state(s).
Reopened 0 state(s).
Evaluated 19627 state(s).
Evaluations: 19627
Generated 38284 state(s).
Dead ends: 0 state(s).
Number of registered states: 19627
Search time: 5.57758s
Total time: 5.60888s
Solution found.
Peak memory: 35620 KB
