INFO     Running translator.
INFO     translator input: ['domain.pddl', 'p02.pddl']
INFO     translator arguments: []
INFO     translator time limit: None
INFO     translator memory limit: None
INFO     callstring: /usr/bin/python /home/TDDC17/sw/fdlog/builds/release32/bin/translate/translate.py domain.pddl p02.pddl
Parsing...
Parsing: [0.080s CPU, 0.082s wall-clock]
Normalizing task... [0.000s CPU, 0.001s wall-clock]
Instantiating...
Generating Datalog program... [0.010s CPU, 0.012s wall-clock]
Normalizing Datalog program...
Normalizing Datalog program: [0.000s CPU, 0.004s wall-clock]
Preparing model... [0.080s CPU, 0.081s wall-clock]
Generated 23 rules.
Computing model... [0.280s CPU, 0.276s wall-clock]
7333 relevant atoms
12880 auxiliary atoms
20213 final queue length
23431 total queue pushes
Completing instantiation... [0.130s CPU, 0.125s wall-clock]
Instantiating: [0.500s CPU, 0.506s wall-clock]
Computing fact groups...
Finding invariants...
9 initial candidates
Finding invariants: [0.010s CPU, 0.004s wall-clock]
Checking invariant weight... [0.000s CPU, 0.003s wall-clock]
Instantiating groups... [0.000s CPU, 0.003s wall-clock]
Collecting mutex groups... [0.000s CPU, 0.000s wall-clock]
Choosing groups...
0 uncovered facts
Choosing groups: [0.000s CPU, 0.000s wall-clock]
Building translation key... [0.000s CPU, 0.000s wall-clock]
Computing fact groups: [0.010s CPU, 0.012s wall-clock]
Building STRIPS to SAS dictionary... [0.000s CPU, 0.000s wall-clock]
Building dictionary for full mutex groups... [0.000s CPU, 0.000s wall-clock]
Building mutex information...
Building mutex information: [0.010s CPU, 0.000s wall-clock]
Translating task...
Processing axioms...
Simplifying axioms... [0.000s CPU, 0.000s wall-clock]
Processing axioms: [0.000s CPU, 0.004s wall-clock]
Translating task: [0.070s CPU, 0.071s wall-clock]
0 effect conditions simplified
0 implied preconditions added
Detecting unreachable propositions...
0 operators removed
0 axioms removed
9 propositions removed
Detecting unreachable propositions: [0.050s CPU, 0.044s wall-clock]
Reordering and filtering variables...
9 of 9 variables necessary.
0 of 9 mutex groups necessary.
1732 of 1732 operators necessary.
0 of 0 axiom rules necessary.
Reordering and filtering variables: [0.000s CPU, 0.008s wall-clock]
Translator variables: 9
Translator derived variables: 0
Translator facts: 157
Translator goal facts: 7
Translator mutex groups: 0
Translator total mutex groups size: 0
Translator operators: 1732
Translator axioms: 0
Translator task size: 8735
Translator peak memory: 62056 KB
Writing output... [0.010s CPU, 0.049s wall-clock]
Done! [0.740s CPU, 0.778s wall-clock]
INFO     Running search (release32).
INFO     search input: output.sas
INFO     search arguments: ['--heuristic', 'hff=ff()', '--search', 'eager_greedy([hff])ls']
INFO     search time limit: None
INFO     search memory limit: None
INFO     search executable: /home/TDDC17/sw/fdlog/builds/release32/bin/downward
INFO     callstring: /home/TDDC17/sw/fdlog/builds/release32/bin/downward --heuristic 'hff=ff()' --search 'eager_greedy([hff])ls' --internal-plan-file sas_plan < output.sas
reading input... [t=0.000128128s]
done reading input! [t=0.0292268s]
packing state variables...done! [t=0.0292676s]
Variables: 9
FactPairs: 157
Bytes per state: 4
Building successor generator...done! [t=0.03081s]
done initializing global data [t=0.0308179s]
Simplifying 3366 unary operators... done! [3366 unary operators]
Initializing additive heuristic...
Initializing FF heuristic...
Conducting best first search without reopening closed nodes, (real) bound = 2147483647
New best heuristic value for ff: 18
[g=0, 1 evaluated, 0 expanded, t=0.0407526s, 4944 KB]
Initial heuristic value for ff: 18
pruning method: none
New best heuristic value for ff: 17
[g=1, 5 evaluated, 1 expanded, t=0.0416973s, 4944 KB]
New best heuristic value for ff: 16
[g=3, 12 evaluated, 3 expanded, t=0.0431941s, 4944 KB]
New best heuristic value for ff: 15
[g=4, 19 evaluated, 4 expanded, t=0.0447816s, 4944 KB]
New best heuristic value for ff: 14
[g=5, 20 evaluated, 5 expanded, t=0.0453623s, 4944 KB]
New best heuristic value for ff: 13
[g=6, 36 evaluated, 7 expanded, t=0.0486541s, 4944 KB]
New best heuristic value for ff: 12
[g=7, 44 evaluated, 8 expanded, t=0.0503674s, 4944 KB]
New best heuristic value for ff: 11
[g=9, 54 evaluated, 10 expanded, t=0.0524915s, 4944 KB]
New best heuristic value for ff: 10
[g=10, 63 evaluated, 11 expanded, t=0.0543041s, 4944 KB]
New best heuristic value for ff: 9
[g=11, 71 evaluated, 12 expanded, t=0.0559374s, 4944 KB]
New best heuristic value for ff: 8
[g=12, 79 evaluated, 13 expanded, t=0.057582s, 4944 KB]
New best heuristic value for ff: 7
[g=14, 92 evaluated, 15 expanded, t=0.0600484s, 4944 KB]
New best heuristic value for ff: 6
[g=15, 95 evaluated, 16 expanded, t=0.060651s, 4944 KB]
New best heuristic value for ff: 5
[g=17, 127 evaluated, 21 expanded, t=0.0670041s, 4944 KB]
New best heuristic value for ff: 4
[g=18, 133 evaluated, 22 expanded, t=0.0682926s, 4944 KB]
New best heuristic value for ff: 3
[g=19, 138 evaluated, 23 expanded, t=0.0694141s, 4944 KB]
New best heuristic value for ff: 2
[g=20, 140 evaluated, 24 expanded, t=0.0699253s, 4944 KB]
New best heuristic value for ff: 1
[g=21, 143 evaluated, 25 expanded, t=0.0706751s, 4944 KB]
New best heuristic value for ff: 0
[g=22, 150 evaluated, 26 expanded, t=0.0719857s, 4944 KB]
Solution found!
Actual search time: 0.0314562s [t=0.0723209s]
load p5 t0 l5 (1)
drive t0 l5 l4 level87 level12 level99 (1)
load p6 t0 l4 (1)
load p2 t0 l4 (1)
drive t0 l4 l3 level79 level8 level87 (1)
load p1 t0 l3 (1)
load p0 t0 l3 (1)
drive t0 l3 l0 level61 level18 level79 (1)
unload p6 t0 l0 (1)
unload p5 t0 l0 (1)
load p4 t0 l0 (1)
unload p2 t0 l0 (1)
drive t0 l0 l3 level43 level18 level61 (1)
unload p4 t0 l3 (1)
drive t0 l3 l1 level31 level12 level43 (1)
drive t0 l1 l2 level28 level3 level31 (1)
load p3 t0 l2 (1)
unload p1 t0 l2 (1)
unload p0 t0 l2 (1)
drive t0 l2 l5 level15 level13 level28 (1)
drive t0 l5 l0 level9 level6 level15 (1)
unload p3 t0 l0 (1)
Plan length: 22 step(s).
Plan cost: 22
Expanded 27 state(s).
Reopened 0 state(s).
Evaluated 151 state(s).
Evaluations: 151
Generated 171 state(s).
Dead ends: 13 state(s).
Number of registered states: 151
Search time: 0.0319655s
Total time: 0.0724665s
Solution found.
Peak memory: 4944 KB
