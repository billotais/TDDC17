
;; This is a small problem instance for the standard Logistics domain,
;; as defined in "logistic.pddl".

(define (problem L_CUSTOM)
  (:domain logistics)
  (:objects
   city1 city2 city3 city4    ;; there are four cities,
   truck1 truck2 truck3 truck4      ;; one truck in each city,
   airplane1 airplane2                 ;; only one airplane,
   train1 train2 train3 train4             ;; 2 trains
   office1 office2 office3 office4   ;; offices are "non-airport" locations
   airport1 airport2 airport3 ;; airports, one per city,
   station1 station2 station4 ;; 3 train station
   packet1 packet2 packet3 packet4           ;; two packages to be delivered
   small medium big
   )
  (:init
   ;; Type declarations:
   (object packet1) (object packet2) (object packet3) (object packet4)

   ;; Size initialization
   (format small) (format medium) (format big)
   ;; all vehicles must be declared as both "vehicle" and their
   ;; appropriate subtype,
   (vehicle truck1) (vehicle truck2) (vehicle truck3) (vehicle truck4) (vehicle airplane1) (vehicle airplane2) (vehicle train1) (vehicle train2) (vehicle train3) (vehicle train4)
   
   (truck truck1) (truck truck2) (truck truck3) (truck truck4) (airplane airplane1) (airplane airplane2) (train train1) (train train2) (train train3) (train train4)

   ;; likewise, airports must be declared both as "location" and as
   ;; the subtype "airport",
   (location office1) (location office2) (location office3) (location office4)
   (location airport1) (location airport2) (location airport3)
   (location station1) (location station2) (location station4)
   (airport airport1) (airport airport2) (airport airport3)
   (station station1) (station station2) (station station4)
   (city city1) (city city2) (city city3) (city city4)

   ;; "loc" defines the topology of the problem,
   (loc office1 city1) (loc airport1 city1) (loc station1 city1) (loc office2 city2) (loc station2 city2)
   (loc airport2 city2) (loc office3 city3) (loc airport3 city3) (loc office4 city4) (loc station4 city4)

   ;; The actual initial state of the problem, which specifies the
   ;; initial locations of all packages and all vehicles:
   (at packet1 office1)
   (at packet2 office3)
   (at packet3 office2)
   (at packet4 station2)
   (at truck1 airport1)
   (at truck2 airport2)
   (at truck3 office3)
   (at truck4 station4)
   (at airplane1 airport1)
   (at airplane1 airport2)
   (at train1 station1)
   (at train2 station2)
   (at train3 station4)
   (at train4 station4)

   (size packet1 small)
   (size packet2 big)
   (size packet3 medium)
   (size packet4 medium)

   (space truck1 big)
   (space truck2 big)
   (space truck3 big)
   (space truck4 big)
   (space truck1 medium)
   (space truck2 medium)
   (space truck3 medium)
   (space truck4 medium)
   (space truck1 small)
   (space truck2 small)
   (space truck3 small)
   (space truck4 small)

   (space airplane1 big)
   (space airplane1 medium)
   (space airplane1 small)
   (space airplane2 small)

   (space train1 small)
   (space train2 medium)
   (space train2 small)
   (space train3 medium)
   (space train3 small)
   (space train4 big)
   (space train4 medium)
   (space train4 small)

   
   )

  ;; The goal is to have both packages delivered to their destinations:
  (:goal (and (at packet1 office2) (at packet2 office1) (at packet3 office2) (at packet4 office4)))
  )
