
ff: parsing domain file
domain 'EMERGENCY' defined
 ... done.
ff: parsing problem file
problem 'EMERGENCY_3P' defined
 ... done.



Cueing down from goal distance:   15 into depth [1][2]
                                  14            [1][2]

Enforced Hill-climbing failed !
switching to Best-first Search now.

advancing to distance :   15
                          14
                          13
                          12
                          11
                          10
                           9
                           8
                           7
                           6
                           5
                           4
                           3
                           2
                           1
                           0

ff: found legal plan as follows

step    0: LOAD_DEPOT FOOD HELICOPTER2 DEPOT1
        1: LOAD_DEPOT FOOD HELICOPTER1 DEPOT1
        2: FLY HELICOPTER1 DEPOT1 PERSON1
        3: FLY HELICOPTER2 DEPOT1 PERSON4
        4: UNLOAD FOOD HELICOPTER2 PERSON4
        5: UNLOAD FOOD HELICOPTER1 PERSON1
        6: FLY HELICOPTER1 PERSON1 DEPOT1
        7: FLY HELICOPTER2 PERSON4 PERSON2
        8: LOAD_DEPOT FOOD HELICOPTER1 DEPOT1
        9: FLY HELICOPTER1 DEPOT1 PERSON2
       10: FLY HELICOPTER2 PERSON2 DEPOT1
       11: UNLOAD FOOD HELICOPTER1 PERSON2
       12: FLY HELICOPTER1 PERSON2 DEPOT1
       13: LOAD_DEPOT FOOD HELICOPTER2 DEPOT1
       14: LOAD_DEPOT WATER HELICOPTER1 DEPOT1
       15: FLY HELICOPTER1 DEPOT1 PERSON2
       16: UNLOAD WATER HELICOPTER1 PERSON2
       17: FLY HELICOPTER1 PERSON2 DEPOT1
       18: LOAD_DEPOT WATER HELICOPTER1 DEPOT1
       19: FLY HELICOPTER1 DEPOT1 PERSON1
       20: UNLOAD WATER HELICOPTER1 PERSON1
       21: FLY HELICOPTER1 PERSON1 DEPOT1
       22: LOAD_DEPOT MEDICINE HELICOPTER1 DEPOT1
       23: FLY HELICOPTER1 DEPOT1 PERSON1
       24: UNLOAD MEDICINE HELICOPTER1 PERSON1
       25: FLY HELICOPTER1 PERSON1 DEPOT1
       26: LOAD_DEPOT WATER HELICOPTER1 DEPOT1
       27: FLY HELICOPTER1 DEPOT1 PERSON3
       28: UNLOAD WATER HELICOPTER1 PERSON3
       29: FLY HELICOPTER1 PERSON3 DEPOT1
       30: LOAD_DEPOT MEDICINE HELICOPTER1 DEPOT1
       31: FLY HELICOPTER1 DEPOT1 PERSON3
       32: UNLOAD MEDICINE HELICOPTER1 PERSON3
     

time spent:    0.00 seconds instantiating 104 easy, 0 hard action templates
               0.00 seconds reachability analysis, yielding 140 facts and 104 actions
               0.00 seconds creating final representation with 44 relevant facts
               0.00 seconds building connectivity graph
               0.00 seconds searching, evaluating 691 states, to a max depth of 2
               0.00 seconds total time

