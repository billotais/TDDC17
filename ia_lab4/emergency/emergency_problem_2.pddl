(define (problem emergency_3p)
   
    (:domain emergency)
    (:objects
        water food medicine clothes
        depot1
        helicopter1 helicopter2
        person1 person2 person3 person4 person5
    )
    (:init
        (crate water) (crate food) (crate medicine) (crate clothes)
        (depot depot1)
        (person person1) (person person2) (person person3) (person person4) (person person5)

        (location person1) (location person2) (location person3) (location person4) (location person5) 
        (location depot1)

        (helicopter helicopter1) (helicopter helicopter2)
        (empty helicopter1) (empty helicopter2)

        (at helicopter1 depot1) (at helicopter2 depot1)

        (at_loc water depot1) (at_loc food depot1) (at_loc medicine depot1) (at_loc clothes depot1)

   
    )

    ;; The goal is to have both packages delivered to their destinations:
    (:goal (and (at_loc water person1) (at_loc food person1) (at_loc medicine person1) (at_loc clothes person1)
                (at_loc water person2) (at_loc food person2)
                (at_loc water person3) (at_loc medicine person3)
                (at_loc food person4) (at_loc clothes person4)
                (at_loc food person5) (at_loc medicine person5) (at_loc clothes person5)
           )
    )
)