(define (domain emergency)
    (:requirements :strips)
    (:predicates
    
        (crate ?c)
        (helicopter ?h) (location ?l) (depot ?d) (person ?p)
        

        (empty ?e)
        (at ?h ?l) ;; ?h (helicopter) is at location ?l
        (in ?c ?h) ;; ?c (crate) is in helicopter ?h
        (at_loc ?c ?l) ;; crate ?c at location ?l
    )
    (:action load_depot
        :parameters (?c ?h ?l) ;; load crate ?c in helicopter ?h at location ?l
        :precondition (and (crate ?c) (helicopter ?h) (location ?l) 
                            (depot ?l) (at ?h ?l) (empty ?h) (at_loc ?c ?l))
        :effect (and (not (empty ?h)) (in ?c ?h))
    )
    (:action load
        :parameters (?c ?h ?l) ;; load crate ?c in helicopter ?h at location ?l
        :precondition (and (crate ?c) (helicopter ?h) (location ?l) 
                            (person ?l) (at ?h ?l) (empty ?h) (at_loc?c ?l))
        :effect (and (not (empty ?h)) (not (at_loc?c ?l)) (in ?c ?h))
    )
    (:action unload
        :parameters (?c ?h ?l) ;; load crate ?c in helicopter ?h at location ?l
        :precondition (and (crate ?c) (helicopter ?h) (location ?l) 
                            (person ?l) (at ?h ?l) (not(empty ?h)) (in ?c ?h))
        :effect (and (empty ?h) (at_loc ?c ?l) (not (in ?c ?h)))
    )
    (:action fly
        :parameters (?h ?l1 ?l2) ;; load crate ?c in helicopter ?h at location ?l
        :precondition (and (helicopter ?h) (location ?l1) (location ?l2)
                             (at ?h ?l1) )
        :effect (and (not (at ?h ?l1)) (at ?h ?l2))
    )


    




)