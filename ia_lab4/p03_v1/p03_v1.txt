INFO     Running translator.
INFO     translator input: ['domain.pddl', 'p03.pddl']
INFO     translator arguments: []
INFO     translator time limit: None
INFO     translator memory limit: None
INFO     callstring: /usr/bin/python /home/TDDC17/sw/fdlog/builds/release32/bin/translate/translate.py domain.pddl p03.pddl
Parsing...
Parsing: [0.350s CPU, 0.346s wall-clock]
Normalizing task... [0.010s CPU, 0.006s wall-clock]
Instantiating...
Generating Datalog program... [0.070s CPU, 0.075s wall-clock]
Normalizing Datalog program...
Normalizing Datalog program: [0.010s CPU, 0.004s wall-clock]
Preparing model... [0.370s CPU, 0.368s wall-clock]
Generated 23 rules.
Computing model... [0.710s CPU, 0.716s wall-clock]
21269 relevant atoms
38868 auxiliary atoms
60137 final queue length
67555 total queue pushes
Completing instantiation... [0.300s CPU, 0.291s wall-clock]
Instantiating: [1.480s CPU, 1.475s wall-clock]
Computing fact groups...
Finding invariants...
9 initial candidates
Finding invariants: [0.000s CPU, 0.006s wall-clock]
Checking invariant weight... [0.020s CPU, 0.014s wall-clock]
Instantiating groups... [0.010s CPU, 0.011s wall-clock]
Collecting mutex groups... [0.000s CPU, 0.000s wall-clock]
Choosing groups...
0 uncovered facts
Choosing groups: [0.000s CPU, 0.002s wall-clock]
Building translation key... [0.000s CPU, 0.001s wall-clock]
Computing fact groups: [0.030s CPU, 0.038s wall-clock]
Building STRIPS to SAS dictionary... [0.000s CPU, 0.000s wall-clock]
Building dictionary for full mutex groups... [0.000s CPU, 0.000s wall-clock]
Building mutex information...
Building mutex information: [0.000s CPU, 0.001s wall-clock]
Translating task...
Processing axioms...
Simplifying axioms... [0.000s CPU, 0.000s wall-clock]
Processing axioms: [0.020s CPU, 0.018s wall-clock]
Translating task: [0.250s CPU, 0.256s wall-clock]
0 effect conditions simplified
0 implied preconditions added
Detecting unreachable propositions...
0 operators removed
0 axioms removed
10 propositions removed
Detecting unreachable propositions: [0.160s CPU, 0.150s wall-clock]
Reordering and filtering variables...
10 of 10 variables necessary.
0 of 10 mutex groups necessary.
3894 of 3894 operators necessary.
0 of 0 axiom rules necessary.
Reordering and filtering variables: [0.020s CPU, 0.020s wall-clock]
Translator variables: 10
Translator derived variables: 0
Translator facts: 252
Translator goal facts: 8
Translator mutex groups: 0
Translator total mutex groups size: 0
Translator operators: 3894
Translator axioms: 0
Translator task size: 19612
Translator peak memory: 96060 KB
Writing output... [0.050s CPU, 0.081s wall-clock]
Done! [2.360s CPU, 2.384s wall-clock]
INFO     Running search (release32).
INFO     search input: output.sas
INFO     search arguments: ['--heuristic', 'hff=ff()', '--search', 'eager_greedy([hff])ls']
INFO     search time limit: None
INFO     search memory limit: None
INFO     search executable: /home/TDDC17/sw/fdlog/builds/release32/bin/downward
INFO     callstring: /home/TDDC17/sw/fdlog/builds/release32/bin/downward --heuristic 'hff=ff()' --search 'eager_greedy([hff])ls' --internal-plan-file sas_plan < output.sas
reading input... [t=0.000123633s]
done reading input! [t=0.0507848s]
packing state variables...done! [t=0.0508091s]
Variables: 10
FactPairs: 252
Bytes per state: 8
Building successor generator...done! [t=0.0529728s]
done initializing global data [t=0.052979s]
Simplifying 7660 unary operators... done! [7504 unary operators]
Initializing additive heuristic...
Initializing FF heuristic...
Conducting best first search without reopening closed nodes, (real) bound = 2147483647
New best heuristic value for ff: 23
[g=0, 1 evaluated, 0 expanded, t=0.0661588s, 6128 KB]
Initial heuristic value for ff: 23
pruning method: none
New best heuristic value for ff: 22
[g=2, 9 evaluated, 2 expanded, t=0.0685453s, 6128 KB]
New best heuristic value for ff: 21
[g=3, 13 evaluated, 3 expanded, t=0.0697038s, 6128 KB]
New best heuristic value for ff: 20
[g=5, 20 evaluated, 5 expanded, t=0.071984s, 6128 KB]
New best heuristic value for ff: 19
[g=6, 27 evaluated, 6 expanded, t=0.0743705s, 6128 KB]
New best heuristic value for ff: 18
[g=7, 33 evaluated, 7 expanded, t=0.07651s, 6128 KB]
New best heuristic value for ff: 17
[g=8, 39 evaluated, 8 expanded, t=0.0786386s, 6128 KB]
New best heuristic value for ff: 16
[g=10, 48 evaluated, 10 expanded, t=0.0817104s, 6128 KB]
New best heuristic value for ff: 15
[g=46, 269 evaluated, 47 expanded, t=0.141519s, 6128 KB]
New best heuristic value for ff: 14
[g=50, 295 evaluated, 53 expanded, t=0.147027s, 6128 KB]
New best heuristic value for ff: 13
[g=51, 301 evaluated, 54 expanded, t=0.148356s, 6128 KB]
New best heuristic value for ff: 12
[g=17, 595 evaluated, 114 expanded, t=0.21615s, 6396 KB]
New best heuristic value for ff: 11
[g=18, 603 evaluated, 115 expanded, t=0.217874s, 6396 KB]
New best heuristic value for ff: 10
[g=21, 621 evaluated, 119 expanded, t=0.221541s, 6396 KB]
New best heuristic value for ff: 9
[g=22, 631 evaluated, 121 expanded, t=0.223605s, 6396 KB]
New best heuristic value for ff: 8
[g=23, 641 evaluated, 123 expanded, t=0.225701s, 6396 KB]
New best heuristic value for ff: 7
[g=24, 647 evaluated, 124 expanded, t=0.227046s, 6528 KB]
New best heuristic value for ff: 5
[g=25, 660 evaluated, 126 expanded, t=0.230461s, 6528 KB]
New best heuristic value for ff: 4
[g=28, 676 evaluated, 129 expanded, t=0.234744s, 6528 KB]
New best heuristic value for ff: 3
[g=29, 680 evaluated, 130 expanded, t=0.235929s, 6528 KB]
New best heuristic value for ff: 2
[g=29, 5240 evaluated, 1294 expanded, t=1.62508s, 13788 KB]
New best heuristic value for ff: 1
[g=30, 5242 evaluated, 1295 expanded, t=1.62589s, 13788 KB]
New best heuristic value for ff: 0
[g=31, 5248 evaluated, 1296 expanded, t=1.62818s, 13788 KB]
Solution found!
Actual search time: 1.56204s [t=1.62832s]
drive t0 l2 l3 level164 level16 level180 (1)
drive t0 l3 l6 level148 level16 level164 (1)
load p4 t0 l6 (1)
drive t0 l6 l1 level145 level3 level148 (1)
load p7 t0 l1 (1)
load p5 t0 l1 (1)
unload p4 t0 l1 (1)
load p1 t0 l1 (1)
drive t0 l1 l6 level142 level3 level145 (1)
unload p7 t0 l6 (1)
drive t0 l6 l5 level124 level18 level142 (1)
unload p1 t0 l5 (1)
drive t0 l5 l6 level106 level18 level124 (1)
drive t0 l6 l1 level103 level3 level106 (1)
drive t0 l1 l0 level91 level12 level103 (1)
load p3 t0 l0 (1)
drive t0 l0 l1 level79 level12 level91 (1)
unload p3 t0 l1 (1)
drive t0 l1 l4 level66 level13 level79 (1)
unload p5 t0 l4 (1)
load p2 t0 l4 (1)
load p0 t0 l4 (1)
drive t0 l4 l2 level54 level12 level66 (1)
drive t0 l2 l7 level36 level18 level54 (1)
load p6 t0 l7 (1)
unload p2 t0 l7 (1)
drive t0 l7 l5 level26 level10 level36 (1)
drive t0 l5 l6 level8 level18 level26 (1)
unload p6 t0 l6 (1)
drive t0 l6 l1 level5 level3 level8 (1)
unload p0 t0 l1 (1)
Plan length: 31 step(s).
Plan cost: 31
Expanded 1297 state(s).
Reopened 0 state(s).
Evaluated 5248 state(s).
Evaluations: 5248
Generated 7347 state(s).
Dead ends: 2284 state(s).
Number of registered states: 5248
Search time: 1.56735s
Total time: 1.63315s
Solution found.
Peak memory: 13788 KB
INFO     Running translator.
INFO     translator input: ['domain.pddl', 'p03.pddl']
INFO     translator arguments: []
INFO     translator time limit: None
INFO     translator memory limit: None
INFO     callstring: /usr/bin/python /home/TDDC17/sw/fdlog/builds/release32/bin/translate/translate.py domain.pddl p03.pddl
Parsing...
Parsing: [0.230s CPU, 0.233s wall-clock]
Normalizing task... [0.010s CPU, 0.004s wall-clock]
Instantiating...
Generating Datalog program... [0.040s CPU, 0.044s wall-clock]
Normalizing Datalog program...
Normalizing Datalog program: [0.000s CPU, 0.005s wall-clock]
Preparing model... [0.260s CPU, 0.264s wall-clock]
Generated 23 rules.
Computing model... [0.510s CPU, 0.510s wall-clock]
21269 relevant atoms
38868 auxiliary atoms
60137 final queue length
67555 total queue pushes
Completing instantiation... [0.240s CPU, 0.238s wall-clock]
Instantiating: [1.070s CPU, 1.078s wall-clock]
Computing fact groups...
Finding invariants...
9 initial candidates
Finding invariants: [0.010s CPU, 0.006s wall-clock]
Checking invariant weight... [0.010s CPU, 0.012s wall-clock]
Instantiating groups... [0.000s CPU, 0.005s wall-clock]
Collecting mutex groups... [0.000s CPU, 0.000s wall-clock]
Choosing groups...
0 uncovered facts
Choosing groups: [0.000s CPU, 0.001s wall-clock]
Building translation key... [0.000s CPU, 0.000s wall-clock]
Computing fact groups: [0.030s CPU, 0.026s wall-clock]
Building STRIPS to SAS dictionary... [0.000s CPU, 0.000s wall-clock]
Building dictionary for full mutex groups... [0.000s CPU, 0.000s wall-clock]
Building mutex information...
Building mutex information: [0.000s CPU, 0.000s wall-clock]
Translating task...
Processing axioms...
Simplifying axioms... [0.000s CPU, 0.000s wall-clock]
Processing axioms: [0.010s CPU, 0.009s wall-clock]
Translating task: [0.140s CPU, 0.140s wall-clock]
0 effect conditions simplified
0 implied preconditions added
Detecting unreachable propositions...
0 operators removed
0 axioms removed
10 propositions removed
Detecting unreachable propositions: [0.080s CPU, 0.084s wall-clock]
Reordering and filtering variables...
10 of 10 variables necessary.
0 of 10 mutex groups necessary.
3894 of 3894 operators necessary.
0 of 0 axiom rules necessary.
Reordering and filtering variables: [0.020s CPU, 0.014s wall-clock]
Translator variables: 10
Translator derived variables: 0
Translator facts: 252
Translator goal facts: 8
Translator mutex groups: 0
Translator total mutex groups size: 0
Translator operators: 3894
Translator axioms: 0
Translator task size: 19612
Translator peak memory: 96060 KB
Writing output... [0.050s CPU, 0.069s wall-clock]
Done! [1.640s CPU, 1.658s wall-clock]
INFO     Running search (release32).
INFO     search input: output.sas
INFO     search arguments: ['--heuristic', 'hff=ff()', '--search', 'eager_greedy([hff])ls']
INFO     search time limit: None
INFO     search memory limit: None
INFO     search executable: /home/TDDC17/sw/fdlog/builds/release32/bin/downward
INFO     callstring: /home/TDDC17/sw/fdlog/builds/release32/bin/downward --heuristic 'hff=ff()' --search 'eager_greedy([hff])ls' --internal-plan-file sas_plan < output.sas
reading input... [t=0.000121626s]
done reading input! [t=0.0498804s]
packing state variables...done! [t=0.049907s]
Variables: 10
FactPairs: 252
Bytes per state: 8
Building successor generator...done! [t=0.0520565s]
done initializing global data [t=0.0520632s]
Simplifying 7660 unary operators... done! [7504 unary operators]
Initializing additive heuristic...
Initializing FF heuristic...
Conducting best first search without reopening closed nodes, (real) bound = 2147483647
New best heuristic value for ff: 23
[g=0, 1 evaluated, 0 expanded, t=0.0620295s, 6128 KB]
Initial heuristic value for ff: 23
pruning method: none
New best heuristic value for ff: 22
[g=2, 9 evaluated, 2 expanded, t=0.0638308s, 6128 KB]
New best heuristic value for ff: 21
[g=3, 13 evaluated, 3 expanded, t=0.064736s, 6128 KB]
New best heuristic value for ff: 20
[g=5, 20 evaluated, 5 expanded, t=0.0662936s, 6128 KB]
New best heuristic value for ff: 19
[g=6, 27 evaluated, 6 expanded, t=0.0677779s, 6128 KB]
New best heuristic value for ff: 18
[g=7, 33 evaluated, 7 expanded, t=0.0690118s, 6128 KB]
New best heuristic value for ff: 17
[g=8, 39 evaluated, 8 expanded, t=0.0702125s, 6128 KB]
New best heuristic value for ff: 16
[g=10, 48 evaluated, 10 expanded, t=0.0720882s, 6128 KB]
New best heuristic value for ff: 15
[g=46, 269 evaluated, 47 expanded, t=0.109573s, 6128 KB]
New best heuristic value for ff: 14
[g=50, 295 evaluated, 53 expanded, t=0.113153s, 6128 KB]
New best heuristic value for ff: 13
[g=51, 301 evaluated, 54 expanded, t=0.114027s, 6128 KB]
New best heuristic value for ff: 12
[g=17, 595 evaluated, 114 expanded, t=0.163343s, 6396 KB]
New best heuristic value for ff: 11
[g=18, 603 evaluated, 115 expanded, t=0.165616s, 6396 KB]
New best heuristic value for ff: 10
[g=21, 621 evaluated, 119 expanded, t=0.170261s, 6396 KB]
New best heuristic value for ff: 9
[g=22, 631 evaluated, 121 expanded, t=0.172723s, 6396 KB]
New best heuristic value for ff: 8
[g=23, 641 evaluated, 123 expanded, t=0.175191s, 6396 KB]
New best heuristic value for ff: 7
[g=24, 647 evaluated, 124 expanded, t=0.176734s, 6528 KB]
New best heuristic value for ff: 5
[g=25, 660 evaluated, 126 expanded, t=0.179733s, 6528 KB]
New best heuristic value for ff: 4
[g=28, 676 evaluated, 129 expanded, t=0.183206s, 6528 KB]
New best heuristic value for ff: 3
[g=29, 680 evaluated, 130 expanded, t=0.184107s, 6528 KB]
New best heuristic value for ff: 2
[g=29, 5240 evaluated, 1294 expanded, t=1.39264s, 13788 KB]
New best heuristic value for ff: 1
[g=30, 5242 evaluated, 1295 expanded, t=1.39353s, 13788 KB]
New best heuristic value for ff: 0
[g=31, 5248 evaluated, 1296 expanded, t=1.39605s, 13788 KB]
Solution found!
Actual search time: 1.33408s [t=1.3962s]
drive t0 l2 l3 level164 level16 level180 (1)
drive t0 l3 l6 level148 level16 level164 (1)
load p4 t0 l6 (1)
drive t0 l6 l1 level145 level3 level148 (1)
load p7 t0 l1 (1)
load p5 t0 l1 (1)
unload p4 t0 l1 (1)
load p1 t0 l1 (1)
drive t0 l1 l6 level142 level3 level145 (1)
unload p7 t0 l6 (1)
drive t0 l6 l5 level124 level18 level142 (1)
unload p1 t0 l5 (1)
drive t0 l5 l6 level106 level18 level124 (1)
drive t0 l6 l1 level103 level3 level106 (1)
drive t0 l1 l0 level91 level12 level103 (1)
load p3 t0 l0 (1)
drive t0 l0 l1 level79 level12 level91 (1)
unload p3 t0 l1 (1)
drive t0 l1 l4 level66 level13 level79 (1)
unload p5 t0 l4 (1)
load p2 t0 l4 (1)
load p0 t0 l4 (1)
drive t0 l4 l2 level54 level12 level66 (1)
drive t0 l2 l7 level36 level18 level54 (1)
load p6 t0 l7 (1)
unload p2 t0 l7 (1)
drive t0 l7 l5 level26 level10 level36 (1)
drive t0 l5 l6 level8 level18 level26 (1)
unload p6 t0 l6 (1)
drive t0 l6 l1 level5 level3 level8 (1)
unload p0 t0 l1 (1)
Plan length: 31 step(s).
Plan cost: 31
Expanded 1297 state(s).
Reopened 0 state(s).
Evaluated 5248 state(s).
Evaluations: 5248
Generated 7347 state(s).
Dead ends: 2284 state(s).
Number of registered states: 5248
Search time: 1.33935s
Total time: 1.40111s
Solution found.
Peak memory: 13788 KB
