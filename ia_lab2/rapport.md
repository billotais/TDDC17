---
title : "TDDC17 - Artificial Intelligence - Lab 2"
author : [Loïs Bilat, Philippe Hérail]
date : \today 
...

# Task 1

The algorithm that we implemented in `CustomGraphSearch.java` is based on the pseudo-code in **Figure 3.7** of the book. We then modified it a little bit to support the boolean `bInsertFront` given to the class constructor. This result to a code similar to the given pseudo-code wth two particularities. The pseudo-code line

> choose a leaf node and remove it from the frontier

is replaced in the code by 
```java
SearchNode node = frontier.removeFirst();
```
and the lines 

> expand the chosen node, **adding the resulting nodes to the frontier** 
> 
> only if not in the frontier or explored set

are replaced by 

```java
List<GridPos> list_pos = p.getReachableStatesFrom(node.getState());
System.out.println(list_pos.toString());
for (GridPos grid_pos : list_pos) {
    SearchNode n = new SearchNode(grid_pos, node);
	if (!explored.contains(n) && !frontier.contains(n)) {
		if (insertFront) frontier.addNodeToFront(n);
		else frontier.addNodeToBack(n);
	}
}
```
The important detail here is the way we add the nodes to the frontier. 

```java
if (insertFront) frontier.addNodeToFront(n);
else frontier.addNodeToBack(n);
```
If the boolean `insertFront` is true, we add the node to the frontier using `addNodeToFront`, otherwise we use `addNodeToBack`. This will allow us the differentiate the two search algorithm we need to implement, **DepthFirstSearch** and **BreadthFirstSearch**. When using **CustomGraphSearch** from **CustomDepthFirstSearch**, the boolean will be *true*. This will result in a "Last In First Out" queue for the frontier, and therefore create the DepthFirstSearch. On the opposite, when we call **CustomGraphSearch** from **CustomBreadthFirstSearch**, we will have a "Last In Last Out" queue for the frontier, making it a BreadthFirstSearch algorithm.

The rest of the code is a simple "translation" from pseudo-code to Java.

# Task 2
## Question 1
The states were the different possible positions for the vaccum cleaner, and the cleanness of the square, e.g. **(2, 3, CLEAN)**, **(4, 13, DIRTY)**, **(34, 21, CLEAN)**

The actions are the following : Move NORTH, Move EAST, Move SOUTH, Move WEST, SUCK

SInce there is 5 actions, *the branching factor* is **5**.

## Question 2

Breadth-First Search uses a FIFO queue, while Uniform-Cost Search uses a priority queue based on path costs of nodes. However, if the cost of all actions is 1, the closest (in depth) a node is from the root, the sooner it wiil be explored. This is the same as Breadth-First Search.

## Question 3

a) is admissible, because it never over-estimates the real cost to reach the objective. Since **h1** and **h2** are admissible, they do not over-estimate the real cost. And since ``(h1+h2)/2 <= max(h1, h2)``, the new heuristic doesn't over-estimate it either.

b) is not admissible. Let suppose that h1 equals to real cost to the objective. This means that ``2h1`` is greater than the correct cost, and therefore over-estimates it, which is not admissible.

c) is admissible, for a similar reason as in a). The new heuristic is always smaller or equal to **h1** and **h2**, and since both are admissible, ``max(h1, h2)`` is also admissible.

## Question 4

As a cost function (g), we could use the total distance parcoured until now from the root. This is a realisitc representation of the cost and energy used until this moment. 

The heuristic (h) could be the Manhattan distance to the goal square. This would be admissible since it never over-estimates the real cost. Indeed, the Manhattan distance correponds to the fastest way to reach the square for an agent that can only move verticaly and horizontaly, and there is no way to find a faster path.

## Question 5

We will apply the 3 following search algorithms (Depth-first, Breadth-first, A*) to the following problem: We have a graph showing distances by road between cities in Switzerland. 

![Distance between swiss cities](graph.png "Distance between swiss cities")

\
And a table giving the "bird-distance" between cities

|  From\\To |  Basel  |  Luzern |  Bern  | Lausanne |  Sion  | **Aigle** | Montreux |
| --------- | ------: | ------: | -----: | -------: | -----: | ----: | -------: |
| Basel     |       0 |      80 |     69 |      136 |    149 | **146** |      135 |
| Luzern    |      80 |       0 |     66 |      140 |    116 | **131** |      126 |
| Bern      |      69 |      66 |      0 |       78 |     80 |  **79** |       70 |
| Lausanne  |     136 |     140 |     78 |        0 |     63 |  **36** |       24 |
| Sion      |     149 |     116 |     80 |       63 |      0 |  **31** |       41 |
| **Aigle** | **146** | **131** | **79** |   **36** | **31** |   **0** |   **14** |
| Montreux  |     135 |     126 |     70 |       24 |     41 |  **14** |        0 |

We start at **Luzern** and we want to find a way to **Aigle**.

### Depth-First search

The search tree is the following : 

![Search tree for Depth-first](depth.png "Depth-first algorithm search tree")

We can see that we always expand the deepest node. This results in a clearly non-optimal path of **512km** instead of **218km** for the best solution (Luzern-Bern-Montreux-Aigle).

### Breadth-First search

The search tree is the following : 

![Search tree for Breadth-first](breadth.png "Breadth-first algorithm search tree")

In this algorithm, we always expands nodes that were added first (First-in, first-out). This result in a "row by row" exploration, and we can see this on the graph. The gray states are the states we have explored. Before going at a deeper level, we finish exploring all nodes at the current level. The result is still not optimal, but it is better (**243km**).

### A* search

The search tree is the following : 

![Search tree for A*](Astar.png "A* algorithm search tree")

We see that we get the optimal path for this problem. At one point, *Aigle* was a child node, but we didn't take it because the cost function of another node (*Montreux*) was smaller. This is what makes A* optimal, because h(n) never estimates the real cost, we know that a node with a smaller value for its cost function must be better


## Question 6
### Breadth-First Search
It is complete. If the deepest goal node is at a finite depth, the algorithm will eventually discover it after it has discoverd all shallower nodes. The branching factor needs to be finite.

It is optimal if the cost is a nondecreasing function of the depth of the node, otherwise it isn't. This is because breadth-first discovers nodes that are close to the root first, so if the cost is a non-decreasing function, every node it discovers later won't have a better cost. Therefore, if we find a solution, all other solutions we might find later are worse.

### Uniform-Cost Search

It is complete for the same reason as Breadth-First Search.

It is optimal. Indeed, whenever uniform-cost search selects a node *n* for expansion, the optimal path to that node has been found. If there was a better path, it would have been taken instead by the algorithm (uniform-cost picks the nodes with the smallest total cost first). 

### Depth-First Search

It is complete for the graph version. if the depth and the branching factor are finite. It will only stops when every branch has been explored. However it is not complete for the tree version, since it doesn't remember already explored nodes.

It is not optimal: It clearly doesn't check closer nodes first. 

### Depth-Limited Search

It is complete if the depth limit is beyond the shallowest goal. Otherwise, the goal will never be reached making the algorithm not-complete.


It is optimal if the depth limit is smaller or equal to the depth of the shallowest node, if the depth limit it beyond the shallowest goal, it is not optimal for the same reason as **Depth-First Search**.

### Iterative deepening depth-first search 

It is complete just like **Breast-First** when the branching factor is finite for the same reason.

It is optimal just like **Breast-First** for the same reason if the path-cost in a non-decreasing function. The closest nodes will always be discoverd first due to the depth limit increasing one by one.

### Bidirectional search 

It is complete if we use Breadth-first search for both directions and that the branching factor is finite. This is the same reason that for classical Breadth-first search.

It is optimal if we use Breadth-first search and if the cost is a nondecreasing function of the depth of the node. Again, same reason that classical Breadth-first search.

### Greedy best-first search 

It is not complete for the tree version and the graph version with infinite space. Indeed, at some node n, the node that is the closest to the goal (n') might be a dead end, in which case, the algorithm will get stuck in a loop between n and n'. In the graph version with a finite space, the algorithm will remember which nodes are already visited and avoid them, preventing the loop from hapenning.

It is not optimal. The algorithm only looks at the neighbor node that is the closest form the goal. It completely forget the cost to get to that node. Another path with nodes further from the goal might have smaller costs on the edges, resulting in a total samller cost.

### A* search

It is complete because it uses the same algorithm as uniform-cost search, except it uses a different cost function, and uniform-cost is complete.

It is optimal if *h(n)* is admissible for the tree-search version and if *h(n)* is consistant for the graph-search version. The nodes will be discovered in a non-decreasing order of the cost function *f(n)*, and for the same reason as Uniform-cost search, this means that closer nodes will be discovered first, and hence give an optimal solution. We can show that the values of *f(n)* are nondecreasing along any patth the following way: 

Suppose n' is a successor of n. Then *g(n')=g(n)+c(n,a,n')* for some action a, and we have
*f(n')=g(n')+h(n')=g(n)+c(n,a,n')+h(n') >= g(n)+h(n)=f(n)* 
This is true because *h(n) never overestimates the true cost.  

### Iterative-deepening A* 

It is complete and optimal, for the same reasons as A*

### Recursive best-first search 

It is complete for the same reason as Best-first Search.

It is optimal if the heuristic function *h(n)* is admissible, as for A* . This implies that *f(n)* is non-decreasing along any path, and for the same reason as Iterative deepening depth-first search, this means that closer nodes will be discoverd first.

## Question 7

We want to find the closest unknown nodes. For that we need a optimal and complete search algorithm. We could use the Breadth-first search algorithm. It will search for unknown squares, and it more optimal if we go the the closest ones first. We don't need a more complex algorithm since each action has the same cost. Once we have found a goal, we follow the path until we discover what is in the square. We then repeat by finding a new unknown square to explore.
 




