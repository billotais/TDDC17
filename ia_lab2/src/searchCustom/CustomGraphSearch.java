package searchCustom;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import searchShared.NodeQueue;
import searchShared.Problem;
import searchShared.SearchObject;
import searchShared.SearchNode;

import world.GridPos;

public class CustomGraphSearch implements SearchObject {

	private HashSet<SearchNode> explored;
	private NodeQueue frontier;
	protected ArrayList<SearchNode> path;
	private boolean insertFront;

	/**
	 * The constructor tells graph search whether it should insert nodes to front or back of the frontier
	 */
    public CustomGraphSearch(boolean bInsertFront) {
		insertFront = bInsertFront;
    }

	/**
	 * Implements "graph search", which is the foundation of many search algorithms
	 */
	public ArrayList<SearchNode> search(Problem p) {
		// The frontier is a queue of expanded SearchNodes not processed yet
		frontier = new NodeQueue();
		/// The explored set is a set of nodes that have been processed
		explored = new HashSet<SearchNode>();
		// The start state is given
		GridPos startState = (GridPos) p.getInitialState();
		// Initialize the frontier with the start state
		frontier.addNodeToFront(new SearchNode(startState));

		// Path will be empty until we find the goal.
		path = new ArrayList<SearchNode>();

		do {
			if (frontier.isEmpty()) return path;
			// choose a leaf node
			SearchNode node = frontier.removeFirst();

			if (p.isGoalState(node.getState())){
				path = node.getPathFromRoot();
				break;
			}
			// add correct position in expored
			explored.add(node);

			// for all neighboors :
			List<GridPos> list_pos = p.getReachableStatesFrom(node.getState());

			for (GridPos grid_pos : list_pos) {

				//add in the frontier if !explore & !frontier
				SearchNode n = new SearchNode(grid_pos, node);
				if (!explored.contains(n) && !frontier.contains(n)) {
					if (insertFront) frontier.addNodeToFront(n);
					else frontier.addNodeToBack(n);
				}
			}
		} while(true);


		return path;
	}

	/*
	 * Functions below are just getters used externally by the program
	 */
	public ArrayList<SearchNode> getPath() {
		return path;
	}

	public ArrayList<SearchNode> getFrontierNodes() {
		return new ArrayList<SearchNode>(frontier.toList());
	}
	public ArrayList<SearchNode> getExploredNodes() {
		return new ArrayList<SearchNode>(explored);
	}
	public ArrayList<SearchNode> getAllExpandedNodes() {
		ArrayList<SearchNode> allNodes = new ArrayList<SearchNode>();
		allNodes.addAll(getFrontierNodes());
		allNodes.addAll(getExploredNodes());
		return allNodes;
	}

}
