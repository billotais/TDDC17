---
header-includes:
  - \usepackage{mathtools}
title : "TDDC17 - Artificial Intelligence - Lab 5"
author : Loïs Bilat, Philippe Hérail
date : \today
numbersections : true
listings : true
geometry : "left=2cm,right=2cm,top=1cm,bottom
=1cm"
---

# Question 1

## Code description for Angle
To create the different states in the `getStateAngle` function, we used the following simple code.

```java
String state = "";
int state_nb = discretize2(angle, NB_STEPS, -3.14, 3.14);

if (state_nb == MIDDLE) state = "perfect";
else if (state_nb < MIDDLE) state = "minus_" + Integer.toString(MIDDLE - state_nb) + "_";
else if (state_nb > MIDDLE) state = "plus_" + Integer.toString(state_nb - MIDDLE) + "_";
		
return state;
```

We start by discretizing the angle of the rocket into **NB_STEPS** different sets. (**NB_STEPS** was set to 15 in our code). We then have three types of states, perfect, for when the angle is close to 0, minus_x and plus_x for when the angle is either smaller or greater than 0. With x representing how far the angle is from 0.

To define a reward depending on the angle, we used the following code.

```java
int state_nb = discretize2(angle, NB_STEPS, -3.14, 3.14);
		
int diff = Math.abs(MIDDLE - state_nb);

if (diff == 0) return 10;
if (diff > 4) return -10;
return 0;
```

Again, we discretized the angle into NB_STEPS sets. We then computed the absolute value of the difference between the calculated state and the middle state. This allows us to have the same reward for a positive and a negative angle of the same absolute value. When then defined the reward to be 10 for an almost perfect angle, 0, for "not so bad" angles and -10 for bad angles.

## Q-learning description

The Q-learning update is implemented the following way.

```java
double max_q_for_action = getMaxActionQValue(new_state);
				
Qtable.put(prev_stateaction, Qtable.get(prev_stateaction) + alpha(Ntable.get(prev_stateaction))*(previous_reward + GAMMA_DISCOUNT_FACTOR*max_q_for_action - Qtable.get(prev_stateaction)));
```

We start by getting the maximum Q-value we can find at the next state (Representing the best action we could achieve if we went to the next state). 
We then update the current Q-value by adjusting it with This maximum Q-value for the next state. We also add the current state own reward to the new Q-value. 
`alpha(Ntable.get(prev_stateaction))` is used to have a decreasing factor the further we are from the start state the make long paths less important.

We could describe the Q-values as an indicator of how good a state and action that we will take from this state are, considering the Q-value of the next state (= how good the next state will be) and the reward value of the current state. We could say that this is an improved reward function that can see further than just itself, and that can give better recommendation on the action to do.

# Question 2

If we turn off exploring from the start before learning, the rocket will just let itself fall, and will never enable any reactor. The explanation can be found in the function `selectAction`. This function will select an action based on the one with the highest Q-value. However, by default, all Q-values are at 0. It will start by defining action *0* (do nothing) as the highest-Q-Value action. Since all other actions also have Q-values of 0, none of them satisfies the condition `(if (Qval > maxQval))`. Therefore, action 0 will be executed in loop. If we had implemented our function `performAction` in a different order, an other action would have been executed in a loop.

# Question 3

For this question, we need to find a model to approximate the Q-function.

Our Q-function is noted $Q(s,a)$, where

\begin{align*}
    &s = f(angle, vx, vy) \\
    &a = f(left,middle, right)    
\end{align*}

We propose a model with three features : the current angle difference with the perfect position, the horizontal speed and the vertical speed, each associated with the function $\psi_j, 1 \le j \le 3$ , and $2^3=8$ actions, noted $a_i, 1 \le i \le 8$, the number of combination of states for the reactors.

If we want to have a linear approximation of $Q(s,a)$, it must be such that 


$$\widehat{Q}(s,a) = \theta_0 + \theta_1 \phi_1(s,a) + ... + \theta_n \phi_n(s,a) = \theta^T \phi(s,a)$$ {#eq:eq1}

\begin{align*}
\phi(s,a) &= \begin{bmatrix}
                \psi_{a1,1}(s,a) \\
                \psi_{a1,2}(s,a) \\
                \psi_{a1,3}(s,a) \\
                \psi_{a2,1}(s,a) \\
                \vdots \\
                \psi_{a8,3}(s,a) \\
                1
            \end{bmatrix}
            , \quad
            \psi_{a_i,j}(s,a) =
              \begin{cases*}
                \psi_{j}(s) \quad& if $ a = a_i $ \\
                0 \quad& if $ a \ne a_i $ \\
              \end{cases*}
\end{align*}

We can note that the $\phi_n$ function are the component functions of $\phi$, which are in fact the $\psi_{a_i,j}$, but we do not use this notation in the equation @eq:eq1 as it would make it less understandable.

Note that the $\phi_n$ functions need not be linear in $s$ and $a$.

We then need to get samples of the actual reward for a given state $s$.
We update the value of of the parameters $\theta_k$, $1 \le k \le n$, and we note $u_l(s)$ the observed value of the total reward from state s at the $l$th trial. 
We then define the error function as
$$ E_j(s)= \frac{(\widehat{Q}(s,a) - u_l(s))^2}{2} $$ {#eq:errorf}
and apply standard linear regression (as explained in the chapter 18 of the course's book), therefore : 
$$ \theta_k \leftarrow \theta_k - \alpha[R(s) + \gamma \max_{a'} [\widehat{Q}(s',a')] - \widehat{Q}(s,a)]\frac{\partial \widehat{Q}(s,a)}{\partial \theta_k} $$ {#eq:thetachange}

$$ \frac{\partial \widehat{Q}(s,a)}{\partial \theta_k} = \phi_k(s,a) \quad \text{, with : } \phi_0(s,a) = 1 \quad \forall (s,a) $$ {#eq:grad}

# Question 4

If the state is defined by the raw values of all the pixels, we could represent the Q-function the following way. For all possible action at some state $s$, we compute the resulting $s'(a)$. This gives us two images, and we can compute the difference between the pixels of these two images. We then sum all the differences, and obtains a number that represent how much the image changed between state $s$ and state $s'(a)$. The higher this value is, the lower the Q-value $Q(s, a)$ will be, since a big change means that the rocket moved a lot, which is the opposite of what we want. 

If we want to include information about the angle in our Q-value, we need to compute the gradient of the image and find the edges of the rocket. With these edges, we can determine the rotation of the rocket. This information would be integrated in the Q-value with the speed information computed earlier . 

\pagebreak

# Code description for part III

The `getStateHover` is very similar to the `getStateAngle` function.

```java
String state = getStateAngle(angle, vx, vy);
		
int vx_nb = discretize(vx, NB_SPEED, -4, 4);
int vy_nb = discretize(vy, NB_SPEED, -4, 4);
		
if (vx_nb == SPEED_NULL) state += "vx_stop_";
else if (vx_nb < SPEED_NULL) state += "vx_left_" + Integer.toString(SPEED_NULL - vx_nb) + "_";
else if (vx_nb > SPEED_NULL) state += "vx_right_" + Integer.toString(vx_nb - SPEED_NULL) + "_";

if (vy_nb == SPEED_NULL) state += "vy_stop_";
else if (vy_nb < SPEED_NULL) state += "vy_down_" + Integer.toString(SPEED_NULL - vx_nb) + "_";
else if (vy_nb > SPEED_NULL) state += "vy_top_" + Integer.toString(vx_nb - SPEED_NULL) + "_";

return state;
```

We start by reusing the angle state, and append to this string the state for the speed, which is done in a similar way. Both speeds are discretized in 9 sets between -4 and 4. We add a string telling in which direction the rocket is moving, with a number indicating how much.

The reward function is a little more complex. 

```java
int state_nb = discretize2(angle, NB_STEPS,-3.14, 3.14);
		
int diff = Math.abs(MIDDLE - state_nb);
double reward = 0;
if (diff == 0) reward = 30;
if (diff == 1) reward = 15;
if (diff > 2) reward = -40;
if (diff > 6) reward = -60;


if (-0.5 <= vx && vx <= 0.5) reward += 20;
else if (vx < -1 || 1 < vx) reward -= 40;
if (-0.5 <= vy && vy <= 0.5) reward += 20;
else if (vy < -1 || 1 < vy) reward -= 40;

return reward;
```

It is split in two parts. The first part assigns a reward depending on the angle, using the same principle as `getAngleReward`. When then add on top of that angle reward two rewards for both speeds. In the end , we have a cumulated reward of the 3 features. This probably isn't the best reward function, but it is good enough to at least see that the rocket is trying of hover. We tried many other function, using dicretize, exponentials, powers of two, we tried to change the parameters, the number of states, etc, but this is what seemed to best the most efficient. We need about 4 millions iteration before seeing a consistent hover that recovers rapidly. Less iterations still show a hover, but not as good.


