public class StateAndReward {

	private static int NB_STEPS = 15; // Must be odd
	private static int NB_SPEED = 9;
	private static int MIDDLE = (int)(NB_STEPS/2);
	private static int SPEED_NULL = (int)(NB_SPEED/2);

	

	/* State discretization function for the angle controller */
	public static String getStateAngle(double angle, double vx, double vy) {

		
		/* TODO: IMPLEMENT THIS FUNCTION */
		String state = "OneStateToRuleThemAll";
		int state_nb = discretize2(angle, NB_STEPS, -3.14, 3.14);

		if (state_nb == MIDDLE) state = "perfect";
		else if (state_nb < MIDDLE) state = "minus_" + Integer.toString(MIDDLE - state_nb) + "_";
		else if (state_nb > MIDDLE) state = "plus_" + Integer.toString(state_nb - MIDDLE) + "_";
		
		return state;
	}

	/* Reward function for the angle controller */
	public static double getRewardAngle(double angle, double vx, double vy) {
	
		int state_nb = discretize2(angle, NB_STEPS, -3.14, 3.14);
		
		int diff = Math.abs(MIDDLE - state_nb);

		if (diff == 0) return 10;
		if (diff > 4) return -10;
		return 0;

	}

	/* State discretization function for the full hover controller */
	public static String getStateHover(double angle, double vx, double vy) {
		// vy bas
		// vx gauche

		String state = getStateAngle(angle, vx, vy);
		
		int vx_nb = discretize(vx, NB_SPEED, -4, 4);
		int vy_nb = discretize(vy, NB_SPEED, -4, 4);
		
		if (vx_nb == SPEED_NULL) state += "vx_stop_";
		else if (vx_nb < SPEED_NULL) state += "vx_left_" + Integer.toString(SPEED_NULL - vx_nb) + "_";
		else if (vx_nb > SPEED_NULL) state += "vx_right_" + Integer.toString(vx_nb - SPEED_NULL) + "_";

		if (vy_nb == SPEED_NULL) state += "vy_stop_";
		else if (vy_nb < SPEED_NULL) state += "vy_down_" + Integer.toString(SPEED_NULL - vx_nb) + "_";
		else if (vy_nb > SPEED_NULL) state += "vy_top_" + Integer.toString(vx_nb - SPEED_NULL) + "_";

		return state;
	}

	/* Reward function for the full hover controller */
	public static double getRewardHover(double angle, double vx, double vy) {

		//return (10 - Math.pow(angle, 2) - Math.pow(vx,2) - Math.pow(vy, 2));
		int state_nb = discretize2(angle, NB_STEPS,-3.14, 3.14);
		
		int diff = Math.abs(MIDDLE - state_nb);
		double reward = 0;
		if (diff == 0) reward = 30;
		if (diff == 1) reward = 15;
		if (diff > 2) reward = -40;
		if (diff > 6) reward = -60;

		
		if (-0.5 <= vx && vx <= 0.5) reward += 20;
		else if (vx < -1 || 1 < vx) reward -= 40;

		if (-0.5 <= vy && vy <= 0.5) reward += 20;
		else if (vy < -1 || 1 < vy) reward -= 40;
		

		
		return reward;
		
	}
	

	// ///////////////////////////////////////////////////////////
	// discretize() performs a uniform discretization of the
	// value parameter.
	// It returns an integer between 0 and nrValues-1.
	// The min and max parameters are used to specify the interval
	// for the discretization.
	// If the value is lower than min, 0 is returned
	// If the value is higher than min, nrValues-1 is returned
	// otherwise a value between 1 and nrValues-2 is returned.
	//
	// Use discretize2() if you want a discretization method that does
	// not handle values lower than min and higher than max.
	// ///////////////////////////////////////////////////////////
	public static int discretize(double value, int nrValues, double min,
			double max) {
		if (nrValues < 2) {
			return 0;
		}

		double diff = max - min;

		if (value < min) {
			return 0;
		}
		if (value > max) {
			return nrValues - 1;
		}

		double tempValue = value - min;
		double ratio = tempValue / diff;

		return (int) (ratio * (nrValues - 2)) + 1;
	}

	// ///////////////////////////////////////////////////////////
	// discretize2() performs a uniform discretization of the
	// value parameter.
	// It returns an integer between 0 and nrValues-1.
	// The min and max parameters are used to specify the interval
	// for the discretization.
	// If the value is lower than min, 0 is returned
	// If the value is higher than min, nrValues-1 is returned
	// otherwise a value between 0 and nrValues-1 is returned.
	// ///////////////////////////////////////////////////////////
	public static int discretize2(double value, int nrValues, double min,
			double max) {
		double diff = max - min;

		if (value < min) {
			return 0;
		}
		if (value > max) {
			return nrValues - 1;
		}

		double tempValue = value - min;
		double ratio = tempValue / diff;

		return (int) (ratio * nrValues);
	}

}
