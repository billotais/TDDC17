package tddc17;


import aima.core.environment.liuvacuum.*;
import aima.core.agent.Action;
import aima.core.agent.AgentProgram;
import aima.core.agent.Percept;
import aima.core.agent.impl.*;


import java.util.Random;

class MyAgentState
{
	public int[][] world = new int[30][30];
	public int initialized = 0;
	final int UNKNOWN 	= 0;
	final int WALL 		= 1;
	final int CLEAR 	= 2;
	final int DIRT		= 3;
	final int HOME		= 4;
	final int ACTION_NONE 			= 0;
	final int ACTION_MOVE_FORWARD 	= 1;
	final int ACTION_TURN_RIGHT 	= 2;
	final int ACTION_TURN_LEFT 		= 3;
	final int ACTION_SUCK	 		= 4;
	
	public int agent_x_position = 1;
	public int agent_y_position = 1;
	public int agent_last_action = ACTION_NONE;
	
	public static final int NORTH = 0;
	public static final int EAST = 1;
	public static final int SOUTH = 2;
	public static final int WEST = 3;
	public int agent_direction = EAST;

	

	
	
	MyAgentState()
	{
		for (int i=0; i < world.length; i++)
			for (int j=0; j < world[i].length ; j++)
				world[i][j] = UNKNOWN;
		world[3][2] = HOME;
		agent_last_action = ACTION_NONE;
		
	}
	// Based on the last action and the received percept updates the x & y agent position
	public void updatePosition(DynamicPercept p)
	{
		Boolean bump = (Boolean)p.getAttribute("bump");

		if (agent_last_action==ACTION_MOVE_FORWARD && !bump)
	    {
			switch (agent_direction) {
			case MyAgentState.NORTH:
				agent_y_position--;
				break;
			case MyAgentState.EAST:
				agent_x_position++;
				break;
			case MyAgentState.SOUTH:
				agent_y_position++;
				break;
			case MyAgentState.WEST:
				agent_x_position--;
				break;
			}
	    }
		
	}
	
	public void updateWorld(int x_position, int y_position, int info)
	{
		world[x_position][y_position] = info;
	}
	
	public void printWorldDebug()
	{
		for (int i=0; i < world.length; i++)
		{
			for (int j=0; j < world[i].length ; j++)
			{
				if (world[j][i]==UNKNOWN)
					System.out.print(" ? ");
				if (world[j][i]==WALL)
					System.out.print(" # ");
				if (world[j][i]==CLEAR)
					System.out.print(" . ");
				if (world[j][i]==DIRT)
					System.out.print(" D ");
				if (world[j][i]==HOME)
					System.out.print(" H ");
			}
			System.out.println("");
		}
	}
}

class MyAgentProgram implements AgentProgram {

	private int initnialRandomActions = 10;
	private Random random_generator = new Random();
	
	// Here you can define your variables!
	public int iterationCounter = 1000;
	public MyAgentState state = new MyAgentState();
	public boolean wall_right = false;
	public boolean wall_bottom = false;
	public boolean wall_left = false;
	public boolean wall_top = false;
	public boolean reached_bot_right = false;
	public boolean correct_direction_end = false;
	public boolean all_wiped = false;
	public int home_x = 0;
	public int home_y = 0;
	// moves the Agent to a random start position
	// uses percepts to update the Agent position - only the position, other percepts are ignored
	// returns a random action
	private Action moveToRandomStartPosition(DynamicPercept percept) {
		int action = random_generator.nextInt(6);
		initnialRandomActions--;
		state.updatePosition(percept);
		if(action==0) {
		    state.agent_direction = ((state.agent_direction-1) % 4);
		    if (state.agent_direction<0) 
		    	state.agent_direction +=4;
		    state.agent_last_action = state.ACTION_TURN_LEFT;
			return LIUVacuumEnvironment.ACTION_TURN_LEFT;
		} else if (action==1) {
			state.agent_direction = ((state.agent_direction+1) % 4);
		    state.agent_last_action = state.ACTION_TURN_RIGHT;
		    return LIUVacuumEnvironment.ACTION_TURN_RIGHT;
		} 
		state.agent_last_action=state.ACTION_MOVE_FORWARD;
		return LIUVacuumEnvironment.ACTION_MOVE_FORWARD;
	}
	
	
	@Override
	public Action execute(Percept percept) {
		
		// DO NOT REMOVE this if condition!!!
    	if (initnialRandomActions>0) {
    		return moveToRandomStartPosition((DynamicPercept) percept);
    	} else if (initnialRandomActions==0) {
    		// process percept for the last step of the initial random actions
    		initnialRandomActions--;
    		state.updatePosition((DynamicPercept) percept);
			System.out.println("Processing percepts after the last execution of moveToRandomStartPosition()");
			state.agent_last_action=state.ACTION_SUCK;
	    	return LIUVacuumEnvironment.ACTION_SUCK;
    	}
		
    	// This example agent program will update the internal agent state while only moving forward.
    	// START HERE - code below should be modified!
    	    	
    	System.out.println("x=" + state.agent_x_position);
    	System.out.println("y=" + state.agent_y_position);
    	System.out.println("dir=" + state.agent_direction);
    	
		
	    iterationCounter--;
	    
	    if (iterationCounter==0)
	    	return NoOpAction.NO_OP;

	    DynamicPercept p = (DynamicPercept) percept;
	    Boolean bump = (Boolean)p.getAttribute("bump");
	    Boolean dirt = (Boolean)p.getAttribute("dirt");
		Boolean home = (Boolean)p.getAttribute("home");
		System.out.println("percept: " + p);
		if (home) {
			home_x = state.agent_x_position;
			home_y = state.agent_y_position;
		}
	    
	    // State update based on the percept value and the last action
	    state.updatePosition((DynamicPercept)percept);
	    if (bump) {
			switch (state.agent_direction) {
			case MyAgentState.NORTH:
				state.updateWorld(state.agent_x_position,state.agent_y_position-1,state.WALL);
				break;
			case MyAgentState.EAST:
				state.updateWorld(state.agent_x_position+1,state.agent_y_position,state.WALL);
				break;
			case MyAgentState.SOUTH:
				state.updateWorld(state.agent_x_position,state.agent_y_position+1,state.WALL);
				break;
			case MyAgentState.WEST:
				state.updateWorld(state.agent_x_position-1,state.agent_y_position,state.WALL);
				break;
			}
		}
		
	    if (dirt)
			state.updateWorld(state.agent_x_position,state.agent_y_position,state.DIRT);
		
	    else
	    	state.updateWorld(state.agent_x_position,state.agent_y_position,state.CLEAR);
	    
	    state.printWorldDebug();
	    
	    
	    // Next action selection based on the percept value
	    if (dirt)
	    {
	    	System.out.println("DIRT -> choosing SUCK action!");
	    	state.agent_last_action=state.ACTION_SUCK;
	    	return LIUVacuumEnvironment.ACTION_SUCK;
	    } 
	    else
	    {
			if (!reached_bot_right) {
				return goToBottomRight();
			}
			else if (!correct_direction_end) {
				return startClean();
			}
			else if (!all_wiped){
				return wipeAll();
			}
			else {
				return goHome();
			}

	    }
	}
	private Action goToBottomRight() {
		System.out.println("Entered reached_bot_right");
		reached_bot_right = wall_bottom && wall_right;
		if (reached_bot_right) {
			for (int i = 0; i <= state.agent_x_position+1; ++i) {
				state.updateWorld(i, 0, state.WALL);
				state.updateWorld(i, state.agent_y_position+1, state.WALL);
			}
			for (int i = 0; i <= state.agent_y_position+1; ++i) {
				state.updateWorld(0, i, state.WALL);
				state.updateWorld(state.agent_x_position+1, i, state.WALL);
			}
			

		}
		if (wall_bottom) {
			state.updateWorld(state.agent_x_position, state.agent_y_position + 1, state.WALL);
		}
		if (wall_right) {
			state.updateWorld(state.agent_x_position + 1, state.agent_y_position, state.WALL);
		}
		if (state.agent_direction == state.WEST) {
			System.out.println("West");
			state.agent_last_action = state.ACTION_TURN_LEFT;
			state.agent_direction = state.SOUTH;
			return LIUVacuumEnvironment.ACTION_TURN_LEFT;
		}
		else if (state.agent_direction == state.NORTH) {
			System.out.println("North");
			state.agent_direction = state.ACTION_TURN_RIGHT;
			state.agent_direction = state.EAST;
			return LIUVacuumEnvironment.ACTION_TURN_RIGHT;
		}

		else if (state.agent_direction == state.EAST) {
			System.out.println("East");
			if (state.world[state.agent_x_position + 1][state.agent_y_position] == state.WALL) {
				state.agent_last_action = state.ACTION_TURN_RIGHT;
				wall_right = true;
				state.agent_direction = state.SOUTH;
				return LIUVacuumEnvironment.ACTION_TURN_RIGHT;
			}
			else {
				state.agent_last_action = state.ACTION_MOVE_FORWARD;
				return LIUVacuumEnvironment.ACTION_MOVE_FORWARD;
			}
		}
		else if (state.agent_direction == state.SOUTH) {
			System.out.println("South");
			if (state.world[state.agent_x_position][state.agent_y_position+1] == state.WALL) {
				state.agent_last_action = state.ACTION_TURN_LEFT;
				wall_bottom = true;
				state.agent_direction = state.EAST;
				return LIUVacuumEnvironment.ACTION_TURN_LEFT;
			}
			else {
				state.agent_last_action = state.ACTION_MOVE_FORWARD;
				return LIUVacuumEnvironment.ACTION_MOVE_FORWARD;
			}
		}

		return LIUVacuumEnvironment.ACTION_MOVE_FORWARD;
	}
	private Action startClean() {
		System.out.println("startCLean");
		System.out.println(state.agent_direction);
		if (state.agent_direction == state.EAST) {
			state.agent_last_action = state.ACTION_TURN_RIGHT;
			state.agent_direction = state.SOUTH;
			return LIUVacuumEnvironment.ACTION_TURN_RIGHT;
		}
		else if (state.agent_direction == state.SOUTH) {
			state.agent_last_action = state.ACTION_TURN_RIGHT;
			state.agent_direction = state.WEST;
			return LIUVacuumEnvironment.ACTION_TURN_RIGHT;
		}
		else {
			System.out.println("Satrt");
			state.agent_last_action = state.ACTION_MOVE_FORWARD;
			correct_direction_end = true;
			return LIUVacuumEnvironment.ACTION_MOVE_FORWARD;
		}


	}
	private Action wipeAll() {
		
		int x = state.agent_x_position;
		int y = state.agent_y_position;

		if (state.world[x][y-1] == state.WALL && (state.world[x-1][y] == state.WALL || state.world[x+1][y] == state.WALL)) {
			int i = 1;
			all_wiped = true;
			while(state.world[i][y] != state.WALL) {
				
				if (state.world[i][y] == state.UNKNOWN || state.world[i][y] == state.DIRT) {
					all_wiped = false;
				}
				++i;				
			}	
		}

		if (state.world[state.agent_x_position-1][state.agent_y_position] == state.WALL) {
			
			if (state.agent_last_action == state.ACTION_MOVE_FORWARD || state.agent_last_action == state.ACTION_SUCK) {
				state.agent_last_action = state.ACTION_TURN_RIGHT;
				state.agent_direction = (state.agent_direction + 1) % 4;
				return LIUVacuumEnvironment.ACTION_TURN_RIGHT;
			}
			else if (state.agent_last_action == state.ACTION_TURN_RIGHT) {
				state.agent_last_action = state.ACTION_MOVE_FORWARD;
				return LIUVacuumEnvironment.ACTION_MOVE_FORWARD;
			}
		}
		if (state.world[state.agent_x_position+1][state.agent_y_position] == state.WALL) {
			if (state.agent_last_action == state.ACTION_MOVE_FORWARD || state.agent_last_action == state.ACTION_SUCK) {
				state.agent_last_action = state.ACTION_TURN_LEFT;
				state.agent_direction = (state.agent_direction + 3) % 4;
				
				return LIUVacuumEnvironment.ACTION_TURN_LEFT;
			}
			else if (state.agent_last_action == state.ACTION_TURN_LEFT) {
				state.agent_last_action = state.ACTION_MOVE_FORWARD;
				
				return LIUVacuumEnvironment.ACTION_MOVE_FORWARD;
			}
		}
		state.agent_last_action = state.ACTION_MOVE_FORWARD;
		return LIUVacuumEnvironment.ACTION_MOVE_FORWARD;
	}
	private Action goHome() {
		System.out.println("Home: " + home_x + " " + home_y);
		if (home_x == state.agent_x_position && home_y == state.agent_y_position) {
			return NoOpAction.NO_OP;
		}
		int direction = getDirectionFromVect(home_x - state.agent_x_position, home_y - state.agent_y_position);
		if (direction == state.agent_direction) {
			state.agent_last_action = state.ACTION_MOVE_FORWARD;
			return LIUVacuumEnvironment.ACTION_MOVE_FORWARD;
		}
		return changeDirection(direction);
	}
	private Action changeDirection(int dest_direction) {
		// When need to turn
		if (((state.agent_direction + 1) % 4) == dest_direction) {
			state.agent_direction = (state.agent_direction + 1) % 4;
			state.agent_last_action = state.ACTION_TURN_RIGHT;
			return LIUVacuumEnvironment.ACTION_TURN_RIGHT;
		}
		else {
			state.agent_direction = (state.agent_direction + 3) % 4;
			state.agent_last_action = state.ACTION_TURN_LEFT;
			return LIUVacuumEnvironment.ACTION_TURN_LEFT;
		}
	}
	private int getDirectionFromVect(int vect_x, int vect_y) {
		
		if (Math.abs(vect_x) >= Math.abs(vect_y)) {
			if (vect_x >= 0) {
				return state.EAST;
			}
			else {
				return state.WEST;
			}
		}
		else  {
			if (vect_y >= 0) {
				return state.SOUTH;
			}
			else {
				return state.NORTH;
			}
		}
		
	}

}

public class MyVacuumAgent extends AbstractAgent {
    public MyVacuumAgent() {
    	super(new MyAgentProgram());
	}
}


