---
title : "TDDC17 - Artificial Intelligence - Lab 1"
author : Philippe Hérail, Loïs Bilat
date : 2017-09-20
titlepage : true
numbersections : true
lof : false
listings : true

geometry : "left=2cm,right=2cm,top=1cm,bottom
=1cm"
---

#TASK 1 : Agent in an obstacle free environment

For this task, we implemented a simple algorithm. The general idea of this algorithm is described by the following

1. Go to the bottom right corner.
2. Then go up by going through each line before moving to the next (See +@fig:agtpath)

![Agent path](task1.png "Agent path"){#fig:agtpath}

3. Then goes home

##Detailed description of the algorithm to go into the bottom right corner
To go to the bottom right corner, we try and find the eastern and southern walls. In order to do so, we go towards each of these direction until we encounter a wall.

When the algorithm detects a wall in one of the previously mentioned directions, then it does not try to continue in this direction anymore but instead tries to go towards the other one (i.e. it goes to the south if it found the eastern wall, otherwise it goes eastwards).

To detect the presence of wall, the algorithm checks the n+1 coordinate in his current direction in his in-memory world map to see if this is a wall.

```java
else if (state.agent_direction == state.EAST) {
    System.out.println("East");
    if (state.world[state.agent_x_position + 1][state.agent_y_position] == state.WALL) {
        state.agent_last_action = state.ACTION_TURN_RIGHT;
        wall_right = true;
        state.agent_direction = state.SOUTH;
        return LIUVacuumEnvironment.ACTION_TURN_RIGHT;
    }
    else {
        state.agent_last_action = state.ACTION_MOVE_FORWARD;
        return LIUVacuumEnvironment.ACTION_MOVE_FORWARD;
    }
}
```

When both the eastern and southern wall have been detected, we set a boolean to true to avoid calling again the function to go to the bottom right, and we use the fact that we know that the world is rectangular, obstacle free and start at coordinates (0,0) to calculate the position of the walls in our world map representation using a simple for loop.
```java
if (reached_bot_right) {
    for (int i = 0; i <= state.agent_x_position+1; ++i) {
        state.updateWorld(i, 0, state.WALL);
        state.updateWorld(i, state.agent_y_position+1, state.WALL);
    }
    for (int i = 0; i <= state.agent_y_position+1; ++i) {
        state.updateWorld(0, i, state.WALL);
        state.updateWorld(state.agent_x_position+1, i, state.WALL);
    }
}
```

##Detailed description of the algorithm to clean the map from the bottom right corner
Then, we start heading up. To do so, our agent gets oriented to the west at first. Then, we go in the direction our agent is facing. When we find a wall in this direction, we turn to the right if there is a wall to the west of the agent, move forward once, then turn right again, and go back to moving forward until the next wall is encountered. If the wall encountered is on the east, then we turn left instead of turning right.

```java
if (state.world[state.agent_x_position-1][state.agent_y_position] == state.WALL) {
    if (state.agent_last_action == state.ACTION_MOVE_FORWARD || state.agent_last_action == state.ACTION_SUCK) {
        state.agent_last_action = state.ACTION_TURN_RIGHT;
        state.agent_direction = (state.agent_direction + 1) % 4;
        return LIUVacuumEnvironment.ACTION_TURN_RIGHT;
    }
    else if (state.agent_last_action == state.ACTION_TURN_RIGHT) {
        state.agent_last_action = state.ACTION_MOVE_FORWARD;
        return LIUVacuumEnvironment.ACTION_MOVE_FORWARD;
    }
}
```

To detect if we have visited all of the map, we test if we are in the top left or right corner (depending on the number of lines in the world), and if all the squares on the line between the two vertical walls have been visited.

```java
int x = state.agent_x_position;
int y = state.agent_y_position;

if (state.world[x][y-1] == state.WALL && (state.world[x-1][y] == state.WALL || state.world[x+1][y] == state.WALL)) {
    int i = 1;
    all_wiped = true;
    while(state.world[i][y] != state.WALL) {

        if (state.world[i][y] == state.UNKNOWN || state.world[i][y] == state.DIRT) {
            all_wiped = false;
        }
        ++i;                
    }   
}
```

##Detailed description of the algorithm to go home after all have been cleaned

To go home, because we know that there no obstacles in our way, we can go via any route to the home square. 

To calculate this route, we compute a vector from our position to the home square.
From this vector, we get the direction to go towards depending on the magnitude on the w and y coordinates
```java
if (Math.abs(vect_x) >= Math.abs(vect_y)) {
    if (vect_x >= 0) {
        return state.EAST;
    }
    else {
        return state.WEST;
    }
}
else  {
    if (vect_y >= 0) {
        return state.SOUTH;
    }
    else {
        return state.NORTH;
    }
}
```

Then we set the direction each time from the returned state from this function until we reach home.

#TASK 2 : Agent in an environment with obstacles

For this task, the implemented algorithm is as follows

1. While the agent has not performed Height\*Length\*2 actions
    1. Check around it for unknown squares, using its world map, in the following order : front, left, right and back.
    2. Set the first unknown square we found as our destination goal, and move towards it

If the agent doesn't find an unknown square in a 30-squares radius, it selects a direction at random.
The agent also select a random direction if it bumps into a wall, which allows it to avoid being stuck. At first, we were testing the diagonals to find a path around it, but it often got stuck in corners because of the fixed order in which it looks for unknown (or at least non-wall) squares. 
It also has a 33% percent chance to select a random direction in to go towards in order to go to places with a lot of unknown cases that may be separated by a line of cleared squares.

```java
// Tell if lines of sight are cleared on every side
boolean[] clear = {true, true, true, true};
// For a radius from 1 to 30
for (int i = 1; i <= 30; ++i) {
    // States of perpendicular cases at radius i
    int states[] = {getStateAtPosition(x,y-i),
        getStateAtPosition(x+i,y), 
        getStateAtPosition(x,y+i),
        getStateAtPosition(x-i,y)};
    // For each side                
    for (int j = 0; j < 4; ++j) {
        int cur_dir = state.agent_direction + order[j];
        // If the case is unknown and the way to it is clear, set it as the goal
        if (states[cur_dir%4] == state.UNKNOWN && clear[cur_dir%4]) return cur_dir%4;
        // If the case is a wall, remember that the line of sight is broken
        if (states[cur_dir%4] == state.WALL) clear[cur_dir%4] = false;
    }           
} 
// If we haven't found anything perpendicular, turn randomly if bump or with 33% chance
if (bump || random_generator.nextInt(3) == 0) {
    return (random_generator.nextInt(2) == 1) ? (state.agent_direction + 1) % 4 : (state.agent_direction + 3) % 4;
}
// Otherwise continue forward
return state.agent_direction;
```

If we compare the scores of the different kind of agents, our agent scores, on a 15*15 grid, after 550 iterations, around 400 points, meaning that most of the time, it clears all of the dirt. 
The default ones seem to be in the -700 to -1000 range, so we can say it is a significant improvement.