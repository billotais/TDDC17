---
title : "TDDC17 - Artificial Intelligence - Lab 3"
author : Philippe Hérail, Loïs Bilat
date : \today

---
All the graphs created and used during this lab are included in ther submission.zip folder.

# Task 2

## Part 5. Questions
1. P(meltdown|!pumpFailWarn,!waterLeakWarn) = 0.02578. To answer this, we defined the values to false on both warning and observed the result on the Meltdown node.
2. Proba meltdown
    * Both warning sensors indicate failure : P(meltdown|pumpFailWarn,waterLeakWarn) = 0.14535
    * P(meltdown|pumpFailure,waterLeak) = 0.2
    * The difference is that the warnings may activate when there is no real problem (or may not activate in case of problem), which make the reliability smaller.
3. Events with severe consequences are hard to acquire stochastic data for. For example, the **meltdown** event given the failures of the cooling systems.
4. If we change the IcyWeather variable to Temperature, then it becomes continuous. Then we have severall alter :
    * Separate the domain of Temperature in two parts : temperature > 0 or temperature < 0. Then we have no changes.
    * Have the probability of Waterleak directly depend of the temperature (for example, constant when temperature < 0 and exponentially decreasing when temperature > 0). Then we need to use **probit** or **logit** distribution

## Part 6. Theory

### a)
Probability table shows the relation between a variable and the ones it depends of.

### b)
We use the following abreviations : *M=Meltdown, PFW=PumpFailureWarning, PF=PumpPailure, WLW=WaterLeakWraning, WL=WaterLeak, IW=IcyWeather*.
```
P(M=F, PFW=F, PF=F, WLW=F, WL=F, IW=F) = 
P(M=F| PFW=F, PF=F, WLW=F, WL=F, IW=F) * 
P(PFW=F| PF=F, WLW=F, WL=F, IW=F) * 
P(PF=F| WLW=F, WL=F, IW=F) * 
P(WLW=F| WL=F, IW=F) * P(WL=F| IW=F) *
P(IW=F) = P(M=F| PF=F, WL=F) * P(PFW=F| PF=F) * 
P(PF=F) * P(WLW=F| WL=F) * P(WL=F| IW=F) * P(IW=F) = 
0.999 * 0.95 * 0.9 * 0.95 * 0.9 * 0.95 = 0.6938 
```
This is a relativly common state to be in (~70%).

### c) 
P(Meldown=T|WL=T, PF=T) = 0.2. Knowing the values of other variables wouldn't change this value. The probability of Meltdown only depends on the state of WaterLeak and PumpFailure. Since those two variables are known, Melltdown can be deduced from them.

### d)

```
P(M=T| PFW=F, WLW=F, WL=F, IW=F) = a * P(M=T, PFW=F, WLW=F, WL=F, IW=F) = 

a * [P(M=T, PFW=F, PF=T, WLW=F, WL=F, IW=F) +
     P(M=T, PFW=F, PF=F, WLW=F, WL=F, IW=F)] =
a * [P(M=T | WL=F, PF=T) * P(PFW=F|PF=T) * P(PF=T) * 
     P(WLW=F|WL=F) * P(WL=T|IW=F) * P(IW=F) + 
     P(M=T | WL=F, PF=F) * P(PFW=F|PF=F) * P(PF=F) *
     P(WLW=F|WL=F) * P(WL=T|IW=F) * P(IW=F)] =
a * [(0.15 * 0.1 * 0.1 * 0.95 * 0.9 * 0.95) + 
     (0.001 * 0.95 * 0.9 * 0.95 * 0.9 * 0.95)] = 
a * (0.00122 + 0.000694) = a*0.00191

--------------------------------------------------

P(M=F| PFW=F, WLW=F, WL=F, IW=F) = a * P(M=F, PFW=F, WLW=F, WL=F, IW=F) = 
a * [P(M=F, PFW=F, PF=T, WLW=F, WL=F, IW=F) +
     P(M=F, PFW=F, PF=F, WLW=F, WL=F, IW=F)] =
a * [P(M=F | WL=F, PF=T) * P(PFW=F|PF=T) * P(PF=T) * 
     P(WLW=F|WL=F) * P(WL=T|IW=F) * P(IW=F) + 
     P(M=F | WL=F, PF=F) * P(PFW=F|PF=F) * P(PF=F) *
     P(WLW=F|WL=F) * P(WL=T|IW=F) * P(IW=F)] =
a * [(0.85 * 0.1 * 0.1 * 0.95 * 0.9 * 0.95) + 
     (0.999 * 0.95 * 0.9 * 0.95 * 0.9 * 0.95)] = 
a * (0.006903 + 0.6938)= a*0.70068

=> P(M| PFW=F, WLW=F, WL=F, IW=F) = a*<0.00191, 0.70068> 
=> <0.00271, 0.99641>
```
# Task 3

1. His chances of surviving dropped from 0.99001 to 0.98116
2. If we add the bicycle, his chances of survival rise to 0.99505
3. Because bayesian networks model the joint probability distribution, they can effectively model any function in propositional logic, but in such acyclic graph, exact inference computation time is linear with the size of the input, and is intractable in most cases. Stochastic approximation techniques may give good approximation in reasonable time

# Part 4

1. With a pump twice as reliable as the original one, the result is better than with our model of Mr. H-S
2. The probability that Mr. H-S survives is of 0.96102. To answer this question, we added a new node int the graph called *1or2* that depended on **WaterLeakWarning** and **PumpFailureWarning**. *1or2* was true when one of the 2 warnings or both were true. We then simply need to make an observation of "true" on this node an see what what the probability of survival.
3. We did the assumption that the behavior of a person could be predicted using probability, which is clearnly not the case. For example, our state HSAwake should depends on a lot a factors like the time of the day, how much Mr. H-S slept the night before, his mood, etc. A simple probabiity is just not representative of the real values.
4. We could add 4 new nodes, each representing the weather of a past day. The weather of a current day would then depend on all of these past days, and use some formula "predicting" the weather for the current day. 
